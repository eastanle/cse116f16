
	Includes button, menu item, or other aspect of the GUI  to select "Make Accusation"
* At the end of each player's turn, after they run out of moves, the JLabel below the board displays a prompt for the user
  to type the word "accuse" into the command line to make an accusation. 
  
  
  	When "Make Accusation" selected, provides window to enter accusation
* If "accuse" is typed following the correct prompt, a window pops up with the title "Accusation" with three JTextFields and
  a JButton. 


	Window in which accusation entered includes way to specify the person, weapon, & room
* There is one JTextField each for a suspect, weapon, and room, in that order. The initial word in each of the JTextFields
  specifies what should be written into the JTextField by the player. 
  
  
  	Does not allow user to enter person, weapon, & room that is not part of game OR shows error when invalid person, weapon, or room entered
* If any of the three JTextFields contain text that does not exactly match with a card and the JButton labeled 'ACCUSE'is pressed, 
  the Accusation window will remain open and an exception will be thrown in the console saying the input is incorrect. The player
  can press the button as many times as they want if the input is incorrect, but an exception will be thrown as long as 
  the input is incorrect.
  
  
  	Window in which accusation entered includes button for user to select once accusation entered
* To the right of the three JTextFields, there is a JButton labeled 'ACCUSE' that controls the final accusation submission.
  If all three inputs are correct, pressing the JButton labeled 'ACCUSE' will close the accusation window and launch a prompt
  indicating if the player won or lost. The logic for the JButton is handled in the AccuseButtonHandler class. 