package code;
import java.util.*;


/**
 * @author David Olsen
 * @author Elizabeth Stanley
 *
 */
public class Player {

	
	/**
	 * Piece associated with the player.
	 */
	private Piece _piece;
	
	/**
	 * Hand of cards that each player holds. 
	 */
	private List<Card> _hand;
	
	/**
     * String that holds the name of the Player
     */
    private String _name;
    
    /**
     * Boolean that holds whether the player has whether the player lost or not
     */
    private Boolean _lost;
	
    /**
     * Boolean that holds whether the player has whether the player wins or not
     */
    private Boolean _win;
    
    public Player(Piece p){
		this._hand = new ArrayList<Card>();
		this._piece = p;
		this._name = p.toString();
		this._lost=false;
		this._win=false;
	}
    
    
	public Player(Piece p, List<Card> list){
		this._hand = list;
		this._piece = p;
		this._name = p.toString();
		this._lost=false;
		this._win=false;
	}
	
	
    /**
    *
    * @return list of cards that make up a piece's current hand.
    */
   public List<Card> getHand(){
       return this._hand;
   }

	/**
	 *
	 * @param c instance of class card that will be added to the piece's hand.
	 * @return true if the hand does not already contain the card and adding was
	 *         successful, otherwise false.
	 */
	public boolean addCardToHand(Card c) {
		if (!this._hand.contains(c)) {
			this._hand.add(c);
			return true;
		}
		return false;
	}

	/**
	 * @return the piece associated with this player.
	 */
	public Piece getPiece(){
		return this._piece;
	}
	
	/**
	 * 
	 * @return whether the player lost or not
	 */
	public Boolean getLost() {
		return this._lost;
	}
	
	/**
	 * 
	 * @return the player lost the game
	 */
	public Boolean lostGame() {
		this._win=false;
		this._lost=true;
		return _lost;
	}
	
	/**
	 * 
	 * @return whether the player won or not
	 */
	public Boolean getWin() {
		return this._win;
	}
	
	/**
	 * 
	 * @return the player won the game
	 */
	public Boolean winGame() {
		this._win=true;
		this._lost=false;
		return this._win;
	}
	
	
	/**
	 *
	 * @return the name of the player.
	 */
	@Override
	public String toString() {
		return this._name;
	}
  
}
