package code;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import javax.swing.*;
import javax.swing.border.Border;

/**
*
* Class that provides all of the logic for the GUI.
*
* @author David Olsen
* @author Jackson Bell
*/
public class BoardGUI {

	/**
	 *  Private instance of the gameboard(model) used to update the GUI
	 */
	private GameBoard _gameBoard;
	
	/**
	 *  Window that will hold all of the jframes
	 */
	private JFrame _window;
	
	/**
	 *  Panel that will hold all of the gameboard tiles. 
	 */
	private JPanel _boardPanel;
	
	/**
	 * Panel that will hold the game output messages. 
	 */
	private JLabel _messageLabel;
	
	
	
	
	/**
	 * 2D array of JButtons used to display the tiles. 
	 */
	private JButton[][] _grid;
		
	

	/**
	 * @param gameboard  The model gameboard logic being passed into the GUI
	 */
	public BoardGUI(GameBoard gameboard){
		this._gameBoard = gameboard;
	}
	
	
	/**
	 * Initialization of the gameboard and message box. 
	 * 
	 * @author Jackson Bell
	 */
	public void initialize(){
		
		_window = new JFrame();

		_boardPanel = new JPanel();
		
		_boardPanel.setLayout(new GridLayout(26, 26));
		_grid = new JButton[26][26];
		for(int i = 0; i < 26; i++){
			for(int j = 0; j < 26; j++){
				_grid[i][j] = new JButton();
				_grid[i][j].setEnabled(false);
				_boardPanel.add(_grid[i][j]);
			}
		}
		
		_boardPanel.setSize(1000, 1000);
		_boardPanel.setLocation(400, 100);
		_boardPanel.setVisible(true);

		
		//border top
		for(int i = 0; i < 1; i++){
			for(int j = 0; j <= 25; j++){
			_grid[i][j].setBackground(Color.darkGray);
			Border emptyBorder = BorderFactory.createEmptyBorder();
			_grid[i][j].setBorder(emptyBorder);
			}
		}
		
		//border left
		for(int i = 1; i <= 25; i++){
			for(int j = 0; j < 1; j++){
				_grid[i][j].setBackground(Color.darkGray);
				Border emptyBorder = BorderFactory.createEmptyBorder();
				_grid[i][j].setBorder(emptyBorder);
			}
		}	
				
		//border bottom
		for(int i = 25; i <= 25; i++){
			for(int j = 0; j < 26; j++){
				_grid[i][j].setBackground(Color.darkGray);
				Border emptyBorder = BorderFactory.createEmptyBorder();
				_grid[i][j].setBorder(emptyBorder);
			}
		}	
		
		//border right
		for(int i = 1; i < 26; i++){
			for(int j = 25; j < 26; j++){
				_grid[i][j].setBackground(Color.darkGray);
				Border emptyBorder = BorderFactory.createEmptyBorder();
				_grid[i][j].setBorder(emptyBorder);
			}
		}	
		
		//basement
		for(int i = 9; i < 16; i++){
			for(int j = 10; j < 15; j++){
				_grid[i][j].setBackground(Color.darkGray);
				Border emptyBorder = BorderFactory.createEmptyBorder();
				_grid[i][j].setBorder(emptyBorder);
			}
		}
		
		//study
		for(int i = 1; i <= 4; i++){
			for(int j = 1; j < 8; j++){
				_grid[i][j].setBackground(Color.orange);
				_grid[i][j].setOpaque(true);		
				Border emptyBorder = BorderFactory.createEmptyBorder();
				_grid[i][j].setBorder(emptyBorder);
			}	
		}
		
		//library
		for(int i = 7; i <= 11; i++){
			for(int j = 1; j <= 6; j++){
				_grid[i][j].setBackground(Color.orange);
				_grid[i][j].setOpaque(true);		
				Border emptyBorder = BorderFactory.createEmptyBorder();
				_grid[i][j].setBorder(emptyBorder);
			}
		}
		
		//also library
		for(int i = 8; i <= 10; i++){
			for(int j = 7; j < 8; j++){
				_grid[i][j].setBackground(Color.orange);
				_grid[i][j].setOpaque(true);		
				Border emptyBorder = BorderFactory.createEmptyBorder();
				_grid[i][j].setBorder(emptyBorder);
			}
		}
		
		//hall
		for(int i = 1; i <= 7; i++){
			for(int j = 10; j <= 15; j++){
				_grid[i][j].setBackground(Color.orange);
				_grid[i][j].setOpaque(true);		
				Border emptyBorder = BorderFactory.createEmptyBorder();
				_grid[i][j].setBorder(emptyBorder);
			}
		}
		
		//lounge
		for(int i = 1; i <= 6; i++){
			for(int j = 18; j <= 24; j++){
				_grid[i][j].setBackground(Color.orange);
				_grid[i][j].setOpaque(true);		
				Border emptyBorder = BorderFactory.createEmptyBorder();
				_grid[i][j].setBorder(emptyBorder);
			}
		}
		
		//dining room
		for(int i = 10; i <= 15; i++){
			for(int j = 17; j <= 24; j++){
				_grid[i][j].setBackground(Color.orange);
				_grid[i][j].setOpaque(true);		
				Border emptyBorder = BorderFactory.createEmptyBorder();
				_grid[i][j].setBorder(emptyBorder);
			}
		}
		
		//also dining room
		for(int i = 10; i <= 16; i++){
			for(int j = 20; j <= 24; j++){
				_grid[i][j].setBackground(Color.orange);
				_grid[i][j].setOpaque(true);		
				Border emptyBorder = BorderFactory.createEmptyBorder();
				_grid[i][j].setBorder(emptyBorder);
			}
		}
		
		//kitchen
		for(int i = 19; i <= 24; i++){
			for(int j = 19; j <= 24; j++){
				_grid[i][j].setBackground(Color.orange);
				_grid[i][j].setOpaque(true);		
				Border emptyBorder = BorderFactory.createEmptyBorder();
				_grid[i][j].setBorder(emptyBorder);
			}
		}	
		
		//billiard room
		for(int i = 13; i <= 17; i++){
			for(int j = 1; j <= 6; j++){
				_grid[i][j].setBackground(Color.orange);
				_grid[i][j].setOpaque(true);		
				Border emptyBorder = BorderFactory.createEmptyBorder();
				_grid[i][j].setBorder(emptyBorder);
			}
		}
		
		//conservatory
		for(int i = 21; i <= 24; i++){
			for(int j = 1; j <= 6; j++){
				_grid[i][j].setBackground(Color.orange);
				_grid[i][j].setOpaque(true);		
				Border emptyBorder = BorderFactory.createEmptyBorder();
				_grid[i][j].setBorder(emptyBorder);
			}
		}
		
		//also conservatory
				for(int i = 20; i < 21; i++){
					for(int j = 2; j <= 5; j++){
						_grid[i][j].setBackground(Color.orange);
						_grid[i][j].setOpaque(true);		
						Border emptyBorder = BorderFactory.createEmptyBorder();
						_grid[i][j].setBorder(emptyBorder);
					}
				}
		
		//ballroom
				for(int i = 18; i < 24; i++){
					for(int j = 9; j <= 16; j++){
						_grid[i][j].setBackground(Color.orange);
						_grid[i][j].setOpaque(true);		
						Border emptyBorder = BorderFactory.createEmptyBorder();
						_grid[i][j].setBorder(emptyBorder);
					}
				}
				
				
		//also ballroom
				for(int i = 24; i < 25; i++){
					for(int j = 11; j <= 14; j++){
						_grid[i][j].setBackground(Color.orange);
						_grid[i][j].setOpaque(true);		
						Border emptyBorder = BorderFactory.createEmptyBorder();
						_grid[i][j].setBorder(emptyBorder);
					}
				}
				
			
				
		//spawns 				
		Border ee = BorderFactory.createEmptyBorder();
		_grid[1][17].setBackground(Color.red);
		_grid[1][17].setBorder(ee);
		_grid[6][1].setBackground(Color.magenta);
		_grid[6][1].setBorder(ee);
		_grid[25][10].setBackground(Color.green);
		_grid[25][10].setBorder(ee);
		_grid[25][15].setBackground(Color.GRAY);
		_grid[25][15].setBorder(ee);
		_grid[19][1].setBackground(Color.blue);
		_grid[19][1].setBorder(ee);
		_grid[8][24].setBackground(Color.yellow);
		_grid[8][24].setBorder(ee);
		
		
		//DOORS	
		//study
		_grid[5][7].setBackground(Color.LIGHT_GRAY);
		//hall
		_grid[8][13].setBackground(Color.LIGHT_GRAY);
		_grid[8][12].setBackground(Color.LIGHT_GRAY);
		_grid[5][9].setBackground(Color.LIGHT_GRAY);
		//lounge
		_grid[7][18].setBackground(Color.LIGHT_GRAY);
		//dining room
		_grid[9][18].setBackground(Color.LIGHT_GRAY);
		_grid[13][16].setBackground(Color.LIGHT_GRAY);
		//kitchen
		_grid[18][20].setBackground(Color.LIGHT_GRAY);
		//ballroom
		_grid[20][8].setBackground(Color.LIGHT_GRAY);
		_grid[20][17].setBackground(Color.LIGHT_GRAY);
		_grid[17][10].setBackground(Color.LIGHT_GRAY);
		_grid[17][15].setBackground(Color.LIGHT_GRAY);
		//conservatory
		_grid[20][6].setBackground(Color.LIGHT_GRAY);
		//billiard rooms
		_grid[12][2].setBackground(Color.LIGHT_GRAY);
		_grid[16][7].setBackground(Color.LIGHT_GRAY);
		//library
		_grid[9][8].setBackground(Color.LIGHT_GRAY);
		_grid[12][4].setBackground(Color.LIGHT_GRAY);
			
			
		//invalid tiles
		Border emptyBorder = BorderFactory.createEmptyBorder();
		_grid[1][7].setBackground(Color.darkGray);
		_grid[1][7].setBorder(emptyBorder);
		_grid[1][9].setBackground(Color.darkGray);
		_grid[1][9].setBorder(emptyBorder);
		_grid[5][1].setBackground(Color.darkGray);
		_grid[5][1].setBorder(emptyBorder);
		_grid[7][1].setBackground(Color.darkGray);
		_grid[7][1].setBorder(emptyBorder);
		_grid[11][1].setBackground(Color.darkGray);
		_grid[11][1].setBorder(emptyBorder);
		_grid[12][1].setBackground(Color.darkGray);
		_grid[12][1].setBorder(emptyBorder);
		_grid[1][16].setBackground(Color.darkGray);
		_grid[1][16].setBorder(emptyBorder);
		_grid[1][18].setBackground(Color.darkGray);
		_grid[1][18].setBorder(emptyBorder);
		_grid[7][24].setBackground(Color.darkGray);
		_grid[7][24].setBorder(emptyBorder);
		_grid[9][24].setBackground(Color.darkGray);
		_grid[9][24].setBorder(emptyBorder);
		_grid[17][24].setBackground(Color.darkGray);
		_grid[17][24].setBorder(emptyBorder);
		_grid[19][24].setBackground(Color.darkGray);
		_grid[19][24].setBorder(emptyBorder);
		_grid[25][18].setBackground(Color.darkGray);
		_grid[25][18].setBorder(emptyBorder);
		_grid[18][1].setBackground(Color.darkGray);
		_grid[18][1].setBorder(emptyBorder);
		_grid[20][1].setBackground(Color.darkGray);
		_grid[20][1].setBorder(emptyBorder);
		_grid[24][7].setBackground(Color.darkGray);
		_grid[24][7].setBorder(emptyBorder);
		_grid[24][18].setBackground(Color.darkGray);
		_grid[24][18].setBorder(emptyBorder);	
		
		
		
		
		_messageLabel = new JLabel("Welcome to the game!");
		
		
		_window.setSize(900, 900);
		
		_window.add(_boardPanel, BorderLayout.CENTER);
		_window.add(_messageLabel, BorderLayout.SOUTH);

		
		_window.setTitle("Cluedo");
		_window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		_messageLabel.setVisible(true);
		_boardPanel.setVisible(true);
		_window.setVisible(true);
	}
	
	
	/**
	 * Used to update the GUI whenever a player is moved. 
	 * 
	 * 
	 * @param originRow		The row from which the player originated. 
	 * @param originCol		The col from which the player originated.
	 * @param newRow		The row to where the player moved. 
	 * @param newCol		The col to where the player moved. 
	 */
	public void updatePlayerLocation(Piece p, int originRow, int originCol, int newRow, int newCol){
		
		Tile[][] gameBoardTiles = _gameBoard.getGameBoard();
		Tile T = gameBoardTiles[originRow][originCol];
		
		JButton from = this._grid[originRow][originCol];
		JButton to = this._grid[newRow][newCol];
		
		if (T.isRoomTile()){
			from.setBackground(Color.orange);
		}
		else if (T.isDoorTile()){
			from.setBackground(Color.LIGHT_GRAY);
		}
		else {
			from.setBackground(null);
		}
		
		if (p.toString().equals("Miss Scarlet")){
			to.setBackground(Color.red);
		}
		if (p.toString().equals("Colonel Mustard")){
			to.setBackground(Color.yellow);
		}
		if (p.toString().equals("Professor Plum")){
			to.setBackground(Color.magenta);
		}
		if (p.toString().equals("Mr. Green")){
			to.setBackground(Color.green);
		}
		if (p.toString().equals("Mrs. White")){
			to.setBackground(Color.gray);
		}
		if (p.toString().equals("Mrs. Peacock")){
			to.setBackground(Color.blue);
		}
		
		
		List<Player> players = _gameBoard.getPlayers();
		for (Player pl: players){
			Piece pie = pl.getPiece();
			if (pie.getRow() == originRow && pie.getCol() == originCol){
				
				if (pie.toString().equals("Miss Scarlet")){
					from.setBackground(Color.red);
				}
				if (pie.toString().equals("Colonel Mustard")){
					from.setBackground(Color.yellow);
				}
				if (pie.toString().equals("Professor Plum")){
					from.setBackground(Color.magenta);
				}
				if (pie.toString().equals("Mr. Green")){
					from.setBackground(Color.green);
				}
				if (pie.toString().equals("Mrs. White")){
					from.setBackground(Color.gray);
				}
				if (pie.toString().equals("Mrs. Peacock")){
					from.setBackground(Color.blue);
				}
			}
		}		
	}
	
	
	/**
	 * Gives the player option to move or look at hand at start of turn. 
	 * 
	 * @param p 	Player whose turn it is. 
	 */
	public void turnPrompt(Player p){
		_messageLabel.setText("It is " + p.toString() + "'s turn. Type 'hand' to look at your hand printed in the console, type anything else to continue to movement.");
	}
	
	
	
	/**
	 * This is used to update the label with the number of movements a player has based on the die roll. 
	 * 
	 * @param p			Piece whose turn it is. 
	 * @param moves		Number of moves left
	 */
	public void movementPrompt(Piece p, int moves){
		_messageLabel.setText(p.toString() + " has " + moves + " move/s left.(w/s/a/d)");
	}
	
	/**
	 * Gives option of making suggestion. 
	 * 
	 * @param p		Player with the option of making the suggestion. 
	 * @param T		RoomTile from which the player is making the suggestion. 
	 */
	public void suggestionPrompt(Player p, RoomTile T){
		_messageLabel.setText(p.toString() + " is inside " + T + ". Would you like to make a suggestion? Type 'y' to suggest, type anything else to not make a suggestion. ");
	}
	
	
	/**
	 * Gives the option of naming a suspect after making a suggestion in a particular room. 
	 * 
	 * @param p			Player making the suggestion
	 * @param T			RoomTile from where the player is making the suggestion. 
	 */
	public void suggestionSuspectPrompt(Player p, RoomTile T){
		_messageLabel.setText(p.toString() + " is suggesting that " + T + " is the murder location. Name the suspect you suggest committed the murder!");
	}
	
	/**
	 * Gives the option of naming a suspect after making a suggestion in a particular room. 
	 * 
	 * @param p			Player making the suggestion
	 * @param T			RoomTile from where the player is making the suggestion. 
	 */
	public void suggestionWeaponPrompt(Player p, RoomTile T){
		_messageLabel.setText(p.toString() + " is suggesting " + T + " is the murder location. Name the weapon you suggest was used to commit the murder!");
	}
	
	/**
	 * Gives the room, suspect, and weapon being suggested, and prints to GUI. 
	 * 
	 * @param p			Player making the suggestion
	 * @param T			RoomTile from where the player is making the suggestion. 
	 */
	public void suggestionFullPrompt(Player p, String R, String S, String W){
		_messageLabel.setText(p.toString() + " is suggesting " + R + " is the murder location, " + S + " is the suspect, and "+ W + " Is the murder weapon. (Type anything to continue and move the suspect into the room)");
	}
	
	
	/**
	 * Printed to the GUI when nobody can disprove the suggestion. 
	 * 
	 */
	public void suggestionFail(){
		_messageLabel.setText("Nobody else could prove this suggestion false. (Type anything to continue)");
	}
	
	/**
	 * Displays that there was a player moved to the suggested room. 
	 * 
	 */
	public void movedPlayerLocation(Player p1, Player p2, RoomTile R){
		_messageLabel.setText(p2.toString() + " was moved to the location of " + p1.toString() + " in the room " + R.toString() + " (Type anything to continue)");
	}
	
	
	/**
	 * Prompt given to the player who needs to disprove the suggestion. 
	 * 
	 * @param p
	 */
	public void proveWrongPrompt(Player p){
		_messageLabel.setText(p.toString() + " must disprove the suggestion. Their matching cards are printed discretely in the console. Type the index of the card you want to display!");
	}
	
	/**
	 * Prompt given to the player who needs to disprove the suggestion. 
	 * 
	 * @param p		Player proving the suggestion false
	 * @param s		String representation of the card proving the suggestion false. 
	 */
	public void displayCardPrompt(String s, Player p){
		_messageLabel.setText(p.toString() + " has chosen to display  " + s + "  in order to prove the suggestion false. (Type anything to make accusation or continue to the next player's turn) ");
	}
	
	
	/**
	 * Prompt given to the player who needs to disprove the suggestion. 
	 * 
	 * @param p
	 */
	public void playerAlreadyLostPrompt(Player p){
		_messageLabel.setText(p.toString() + " has made a false accusation, and is out of the game. Type anything to proceed to the next player's turn.");
	}
	
	
	/**
	 * Prompt given at end of turn to make an accusation. 
	 * 
	 */
	public void accusationPrompt(){
		_messageLabel.setText("Would you like to make an accusation? If so, type 'accuse', type anything else to continue.");
	}
	
	
	
	/**
	 * Prompt that is displayed when the game is won
	 * 
	 * @param p  Player who won
	 */
	public void winGame(Player p){
		JFrame jf = new JFrame();
		JPanel jp = new JPanel();
		JLabel jl = new JLabel(p.toString() + ", You solved the mystery! Congratulations! You may now close the program.");
		jl.setFont(new Font("Verdana",1,15));
		
		jf.setSize(800, 100);
		jf.setTitle("CLUE");
		jf.setVisible(true);
		jf.setLocation(700, 500);
		jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		jf.setContentPane(jp);
		jp.add(jl);
		jp.setBackground(Color.ORANGE);
		
	}
	
	/**
	 * Prompt that is displayed when the game is lost
	 * 
	 * @param p  Player who lost
	 */
	public void lostGame(Player p){
		JFrame jf = new JFrame();
		JPanel jp = new JPanel();
		JLabel jl = new JLabel(p.toString() + ", You guessed incorrectly! You're out the game! You may close this window, but you can not move or make suggestions.");
		jl.setFont(new Font("Verdana",1,12));
		
		jf.setSize(900, 100);
		jf.setTitle("CLUE");
		jf.setVisible(true);
		jf.setLocation(700, 500);
		//jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		jf.setContentPane(jp);
		jp.add(jl);
		jp.setBackground(Color.ORANGE);
	}
	
	
	
	/**
	 * Prompt that is displayed for a player to make an accusation, with a jbutton that is pressed to make the accusation!
	 * 
	 * @param p  Player who is making accusation
	 */
	public void accusationDisplay(Player p){
		JFrame accFrame = new JFrame();
		JPanel accPan = new JPanel();
		JLabel accLabel = new JLabel(p.toString() + ", make an accusation!");
		accLabel.setFont(new Font("Verdana",1,15));
		accPan.setBackground(Color.ORANGE);
		
		accFrame.setSize(900, 300);
		accFrame.setTitle("Accusation");
		accFrame.setVisible(true);
		accFrame.setLocation(700, 500);
		
		
		
		JTextField field1 = new JTextField(10);
		field1.setText("Suspect");
		JTextField field2 = new JTextField(10);
		field2.setText("Weapon");
		JTextField field3 = new JTextField(10);
		field3.setText("Room");
		
		JButton button  = new JButton("ACCUSE");
		
		accFrame.setContentPane(accPan);
		accPan.add(accLabel);
		accPan.add(field1);
		accPan.add(field2);
		accPan.add(field3);
		accPan.add(button);
				
		button.addActionListener(new AccuseButtonHandler(this._gameBoard, field1, field2, field3, accFrame, p));
		
		accFrame.repaint();
		accPan.repaint();
		accFrame.pack();
	}
	
	
	
	
	
	
}
