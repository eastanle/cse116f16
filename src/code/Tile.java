
package code;

/**
 *
 * Object that sits in an array, used by the move method in the gameboard class to determine if a move is valid.
 * 
 * @author David Olsen
 * @author Jackson Bell
 * @author Elizabeth Stanley
 * @author Amy Lipski
 * 
 */
public class Tile {
    
    /**
     * Value that determines if a Piece can move to the tile's location
     */
    private boolean _canMoveTo;
    
    /**
     *
     * @param b when tile is initialized, decide if the tile should allow movement to it's position, controlled by b.
     */
    public Tile(boolean b){
        this._canMoveTo = b;
    }
    
    /**
     *
     * @return boolean value of does tile allow movement to this position.
     */
    public boolean canMoveTo(){
        return this._canMoveTo;
    }
    
    /**
     *
     * @param b set if this tile should allow movement. If this Tile is a RoomTile, this method does not change anything. 
     */
    public void canMoveSet(boolean b){
        this._canMoveTo = b;
    }
    
    /**
     *
     * @return is this tile a RoomTile or just a standard tile. 
     */
    public boolean isRoomTile(){
        return false;
    }
    
    /**
     *
     * @return does this tile have a secret passage.
     */
    public boolean hasSecretPassage(){
        return false;
    }

    
    
	/**
	 * @return is this tile a door
	 */
	public boolean isDoorTile() {
		return false;
	}
}
