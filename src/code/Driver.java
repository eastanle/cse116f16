package code;

/**
 * @author David Olsen
 *
 *Used to run the program. 
 */
public class Driver { 
	public static void main(String args[]){
		GameBoard game = new GameBoard();
		game.run();
	}
}
