package code;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 * 
 * 
 * Event handler for the accusation button .
 * 
 * 
 * @author David Olsen
 *
 * 
 */
public class AccuseButtonHandler implements ActionListener{

	/**
	 * Gameboard to have an event
	 */
	private GameBoard g;
	
	/**
	 * Passed suspect String
	 */
	private JTextField sus;
	
	/**
	 * Passed weapon String
	 */
	private JTextField wep;
	
	/**
	 * Passed room String
	 */
	private JTextField rom;
	
	/**
	 * JFrame being self-destructed after button is pressed
	 */
	private JFrame frameframe;
	
	/**
	 * Player passed into accusation method
	 */
	private Player p;
	
	
	/**
	 * Constructor for button handler. Handles the button press then self-destructs!
	 * 
	 * @param ga 	Gameboard instance plugged into the eventhandler
	 * @param s1	String representing the suspect
	 * @param s2	String representing the weapon
	 * @param s3	String representing the room
	 * @param frame	 JFrame being self-destructed
	 * @param p	 Player making the accusation
	 */
	public AccuseButtonHandler(GameBoard ga, JTextField field1, JTextField field2, JTextField field3, JFrame frame, Player player){
		g = ga;
		sus = field1;
		wep = field2;
		rom = field3;
		frameframe = frame;
		p = player;
	}
	
	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		
		try{			
			g.accusationHelper(sus.getText(), wep.getText(), rom.getText(), p);
			//MR. SELF DESTRUCT!
			frameframe.setVisible(false);
			frameframe.dispose();
		}
		catch(IllegalInputException ex){
			System.out.println(ex);
		}
		
		
	}

}
