package code;

/**
 *
 * Card class. This class holds the information for each card used in the game. 
 * 
 * @author David Olsen
 * @author Jackson Bell
 * @author Elizabeth Stanley
 * @author Amy Lipski
 */
public class Card {
    
    /**
     * Holds the name of the card
     */
    private String _name;
    
    /**
     * Holds the type (Weapon/Suspect/Room) of the card
     */
    private String _type;
    
    /**
     * 
     * @param name  The specified name of the card. 
     * @param type  The type of the card. 
     */
    public Card(String name, String type){
        this._name = name;
        this._type = type;
    }
    
    /**
     *
     * @return the name of the card.
     */
    public String getName(){
        return this._name;
    }

    /**
     *
     * @return the type of the card (suspect/weapon/room).
     */
    public String getType(){
        return this._type;
    }
    
    /**
     *
     * @return the name of the card and the type of the card.
     */
    @Override
    public String toString(){
        return " name: " + this._name + " type: " + this._type + " ";
    }
}
