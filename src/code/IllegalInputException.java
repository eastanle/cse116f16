package code;

/**
 * 
 * Thrown when there is an error in input not related to movement. 
 *
 * @author David Olsen
 */
public class IllegalInputException extends Exception{
	
	
    /**
     * @param s String passed in as error message.
     */
    public IllegalInputException(String s){
        super(s);
    }

}
