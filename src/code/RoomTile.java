
package code;
import java.util.*;


/**
 *
 * Special case for Tile class. What happens when the Piece enters a room. 
 *
 * @author David Olsen
 * @author Jackson Bell
 * @author Amy Lipski
 * @author Elizabeth Stanley
 */
public class RoomTile extends Tile{
    
    /**
     * Name of the RoomTile, relevant because this must be the same as the room in the suggestion
     */
    private String _roomName;
    
    /**
     * does this RoomTile have a secret passage
     */
    private boolean _hasSecretPassage;
    
    /**
     * Row coordinate of secret passage destination(if there is one)
     */
    private int _secRow;
    
    /**
     * Column coordinate of secret passage destination(if there is one)
     */
    private int _secCol;
    
    /**
     * Special case of tile that controls what happens when a piece enters a room. 
     * 
     * @param name              The name of the room.
     * @param hasSecretPassage  If the room is a corner room, and therefore has a secret passage.
     */
    public RoomTile(String name){
        super(true);
        this._roomName = name;
        this._hasSecretPassage = false;
    }
    
    
    /**
     * Special case of tile that controls what happens when a piece enters a room. Secret passage location specified in second constructor
     * 
     * @param name				The name of the room 
     * @param hasSecretPassage	If the room is a corner room, and therefore has a secret passage.
     * @param row				set row of the secret passage
     * @param col				set column of the secret passage
     */
    public RoomTile(String name, boolean hasSecretPassage, int row, int col){
        super(true);
        this._roomName = name;
        this._hasSecretPassage = hasSecretPassage;
        this._secRow = row;
        this._secCol = col;
    }
    
    /**
     *
     * @param row set row of the secret passage
     * @param col set column of the secret passage
     */
    public void setSecretPassage(int row, int col){
        this._secRow = row;
        this._secCol = col;
    }

    /**
     *
     * @return row of secret passage destination.
     */
    public int getSecretPassageRow(){
        return this._secRow;
    }

    /**
     *
     * @return column of secret passage destination.
     */
    public int getSecretPassageCol(){
        return this._secCol;
    }
    
    
    /* (non-Javadoc)
     * @see code.Tile#canMoveTo()
     */
    @Override
    public boolean canMoveTo(){ 
        return true;
    }
    
    /* (non-Javadoc)
     * @see code.Tile#canMoveSet(boolean)
     */
    @Override
    public void canMoveSet(boolean b){ 
    }
    
    /* (non-Javadoc)
     * @see code.Tile#isRoomTile()
     */
    @Override
    public boolean isRoomTile(){
        return true;
    }
    
    /* (non-Javadoc)
     * @see code.Tile#hasSecretPassage()
     */
    @Override
    public boolean hasSecretPassage(){
        return this._hasSecretPassage;
    }
    
    /**
     *
     * @return name of the room.
     */
    @Override
    public String toString(){
        return this._roomName;
    }
}
