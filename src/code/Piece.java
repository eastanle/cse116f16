
package code;
import java.util.*;


/**
 *  This class represents the data for each piece on the gameboard, that is controlled by each human player.
 * 
 * @author David Olsen
 * @author Elizabeth Stanley
 * @author Jackson Bell
 * @author Amy Lipski
 */
public class Piece {
    
    /**
     * Integer that holds the row coordinate of the Piece Object
     */
    private int _row;
    
    /**
     * Integer that holds the column coordinate of the Piece Object
     */
    private int _col;
    
    /**
     * List of Cards that make up the player's current hand
     */
    private List<Card> _hand;
    
    /**
     * String that holds the name of the Piece
     */
    private String _name;
    
    /**
     * Constructor for piece. 
     * 
     * @param row Intial row of created piece.
     * @param col Intial column of created piece.
     * @param name Name of created piece.
     */
    public Piece(int row, int col, String name){
        this._row = row;
        this._col = col;
        this._hand = new ArrayList<Card>();
        this._name = name;
        
    }
    /**
     *
     * @return current row of piece.
     */
    public int getRow(){
        return this._row;
    }
    /**
     *
     * @return current column of piece.
     */
    public int getCol(){
        return this._col;
    }

    /**
     *
     * @param row set new row for piece.
     */
    public void setRow(int row){
        this._row = row;
    }
    /**
     *
     * @param col set new column for piece.
     */
    public void setCol(int col){
        this._col = col;
    } 
    /**
     *
     * @return the name of the piece.
     */
    @Override
    public String toString(){
        return this._name;
    }
}
