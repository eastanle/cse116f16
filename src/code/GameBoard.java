
package code;

import java.awt.Color;
import java.awt.Font;
import java.util.*;

import javax.swing.*;
/**
 *
 * Class that provides all of the logic for the game.
 *
 * @author David Olsen
 * @author Amy Lipski
 * @author Jackson Bell
 * @author Elizabeth Stanley
 */
public class GameBoard {

	/**
	 * Array of Tile that controls player movement.
	 */
	private Tile[][] _gameBoard;

	/**
	 * List of all pieces added to the game.
	 */
	private List<Piece> _pieces;

	/**
	 * List of all players playing the game
	 */
	private List<Player> _players;

	/**
	 * The list of Cards used by the game.
	 */
	private List<Card> _deck;

	/**
	 * The 3 cards that if correctly guessed allows someone to win the game.
	 */
	private List<Card> _envelope;
	
	/**
	 * The instance of the GUI for this game.
	 */
	private BoardGUI _gui;
	
	/**
	 * Scanner used for player input.
	 */
	private Scanner _reader;

	/**
	 * Used to hold the relevant suspect when the accusation button is pressed. 
	 */
	private Card _lastAccusedSuspect;
	
	/**
	 * Used to hold the relevant weapon when the accusation button is pressed. 
	 */
	private Card _lastAccusedWeapon;
	
	/**
	 * Used to hold the relevant room when the accusation button is pressed. 
	 */
	private Card _lastAccusedRoom;
	
	/**
	 * Arrays of strings for Elizabeth's genDeckEn() method
	 */
	private String[] room    = {"Kitchen", "Ballroom", "Conservatory", "Billiard Room", "Library", "Study", "Hall", "Lounge", "Dining Room"};
	private String[] suspect = {"Miss Scarlet", "Professor Plum", "Mr. Green", "Mrs. White", "Mrs. Peacock", "Colonel Mustard"};
	private String[] weapon  = {"Candlestick", "Dagger", "Lead Pipe", "Revolver", "Rope", "Wrench"};
	
	/**
	 * Arrays of ints for each player's starting location.
	 */
	private int[] suspectRow = { 1,  6, 25, 25, 19,  8};
	private int[] suspectCol = {17,  1,  10, 15, 1, 24};
	
	
	/**
	 * Constructor for the class GameBoard(). Initializes all fields.
	 * 
	 */
	public GameBoard() {
		this._gameBoard = new Tile[27][26];
		this._pieces = new ArrayList<Piece>();
		this._players = new ArrayList<Player>();
		this._deck = new ArrayList<Card>();
		this._envelope = new ArrayList<Card>();
		this._gui = new BoardGUI(this);
		this._reader = new Scanner(System.in);
		this._lastAccusedSuspect = null;
		this._lastAccusedWeapon = null;
		this._lastAccusedRoom = null;
	}

	
	/**
	 * The script for running the game.
	 * 
	 * @author David Olsen
	 */
	public void run(){
		this.buildGameBoard();
		this._gui.initialize();
		
		this.genPieces();
		this.genDeckEnv();
		
		int[] ind = {0,1,2,3,4,5};
		this.genPlayers(ind);		
		
		//Initial print of the cards in the envelope
		System.out.println("Cards in envelope (for testing)");
		for (Card c: this._envelope){
			System.out.println(c);
		}
		
		Player p1 = _players.get(0);	
		
		// Loop for each player turn
		while (true){
			
			//Check to see if player has already made an accusation and failed.
			if(p1.getLost()){
				this._gui.playerAlreadyLostPrompt(p1);
				String youLost = this._reader.next();
				//if player lost, thier turn is skipped, but they still technically exist. 
				p1 = this.turnSequence(p1);
				continue;
			}
			
			int moves = this.dieRoll();
			
			Piece p = p1.getPiece();
			
			this._gui.turnPrompt(p1);
			
			String beginTurn = this._reader.next();
			if (beginTurn.equals("hand")){
				for(Card c: p1.getHand()){
					System.out.println(c);
				}
			}
			
			// loop for number of moves
			while(moves > 0){
				
				int currentRow = p.getRow();
				int currentCol = p.getCol();
				
				Tile currentTile = this._gameBoard[currentRow][currentCol];
				
				if (currentTile.isRoomTile()){
					this._gui.suggestionPrompt(p1, (RoomTile)currentTile);
					String suggestion = this._reader.next();
					if (suggestion.equals("y")){
						this.suggestionDisplay(p1, (RoomTile)currentTile);
						moves = 0;
						continue;
					}
				}
				
				this._gui.movementPrompt(p, moves);
				String input = this._reader.next();
				
				try{
					boolean secretTook = this.move(p, input);
					int movedRow = p.getRow();
					int movedCol = p.getCol();
					
					Tile newTile = this._gameBoard[movedRow][movedCol];
					
					this._gui.updatePlayerLocation(p, currentRow, currentCol, movedRow, movedCol);
					
					if (newTile.isRoomTile() && !currentTile.isRoomTile()){
						RoomTile R = (RoomTile)newTile;
						this._gui.suggestionPrompt(p1, (RoomTile)newTile);
						String suggestion = this._reader.next();
						if (suggestion.equals("y")){
							this.suggestionDisplay(p1, R);
							moves = 0;
						}
						moves = 0;
					}
					else if (newTile.isRoomTile() && currentTile.isRoomTile()){
						RoomTile R = (RoomTile) newTile;
						this._gui.suggestionPrompt(p1, (RoomTile)newTile);
						String suggestion = this._reader.next();
						if (suggestion.equals("y")){
							this.suggestionDisplay(p1, R);
							moves = 0;
						}
						if (secretTook){
							moves = 0;
						}
					}
					else if (!newTile.isRoomTile() && currentTile.isRoomTile()){
						moves--;
					}
					else if (!newTile.isRoomTile() && !currentTile.isRoomTile()){
						moves--;
					}
				}
				catch(IllegalMoveException e){
					System.out.println(e);
				}			
			}
			this._gui.accusationPrompt();
			String inputA = this._reader.next();
			if (inputA.equals("accuse")){
				this.accusationDisplay(p1);				
			}
			p1 = this.turnSequence(p1);
		}		
	}
	

	/**
	 * This method is used to handle the logic for making an accusation. 
	 * 
	 * @param p		Player who is making the accusation
	 */
	public void accusationDisplay(Player p){
		
		this._lastAccusedSuspect = null;
		this._lastAccusedWeapon = null;
		this._lastAccusedRoom = null;
		
		this._gui.accusationDisplay(p);
	}
	
	
	/**
	 * 
	 * Method called when pressing the accusation button - checks to see if the card is legal, and if it is proceeds to finish the accusation. 
	 * 
	 * @param suspect					String representing the suspect
	 * @param weapon					String representing the weapon	
	 * @param room						String representing the room
	 * @throws IllegalInputException	Exceprion thrown and handled by the actionlistener
	 */
	public void accusationHelper(String suspect, String weapon, String room, Player p) throws IllegalInputException{
		this._lastAccusedSuspect = this.suspectChecker(suspect);
		this._lastAccusedWeapon = this.weaponsChecker(weapon);
		this._lastAccusedRoom = this.roomCheckerAccuse(room);
		
		this.accusation(_lastAccusedRoom, _lastAccusedWeapon, _lastAccusedSuspect, p);
		
		if (p.getLost()){
			this._gui.lostGame(p);
		}
		else if (p.getWin()){
			this._gui.winGame(p);
		}			
	}
	
	
	/**
	 * 
	 * This method is used to collect user input for the suggestion and using the methods defined as
	 *  suggestion() and suggestionResults(), we will allow the suggestion procedure to proceed. 
	 *  
	 *  First the references to the card specified by the player are generated. 
	 *  
	 *  Then, the player next in order is given the option to prove the suggestion wrong.
	 *  The player is displayed thier matching cards int the console, and then they pick one to display in the GUI 
	 * 
	 * 
	 * @param p		Player who is making the suggestion
	 * @param R		Room where the player is making the suggestion. 
	 * 
	 * @author David Olsen
	 */
	public void suggestionDisplay(Player p, RoomTile R){
		
		Card roomCard;
		Card suspectCard;
		Card weaponCard;
			
		roomCard = this.roomChecker(R);
		
		while (true){
			this._gui.suggestionSuspectPrompt(p, R);
			String suspect = _reader.nextLine(); 
			try{
				suspectCard = this.suspectChecker(suspect);
				break;
			}
			catch(IllegalInputException e){
				System.out.println(e);
			}
		}
		
		while (true){
			this._gui.suggestionWeaponPrompt(p, R);
			String weapon = _reader.nextLine(); 
			try{
				weaponCard = this.weaponsChecker(weapon);
				break;
			}
			catch(IllegalInputException e){
				System.out.println(e);
			}
		}
		
		this._gui.suggestionFullPrompt(p, roomCard.getName(), suspectCard.getName(), weaponCard.getName());
		String unused1 = this._reader.next();
		
		Player movedPlayer;
		for (Player pmove: this._players){
			if (pmove.toString().equals(suspectCard.getName())){
				movedPlayer = pmove;
				
				// Move suggested player to same location on the gameboard as accusing player. 
				int originalRow = movedPlayer.getPiece().getRow();
				int originalCol = movedPlayer.getPiece().getCol();
				
				
				movedPlayer.getPiece().setRow(p.getPiece().getRow());
				movedPlayer.getPiece().setCol(p.getPiece().getCol());
				
				int newRow =  movedPlayer.getPiece().getRow();
				int newCol =  movedPlayer.getPiece().getCol();
				
				this._gui.updatePlayerLocation(movedPlayer.getPiece(), originalRow, originalCol, newRow, newCol);
				
				this._gui.movedPlayerLocation(p, movedPlayer, R);
				String unused2 = this._reader.next();
				
			}
		}
		
		int resultOfSuggestion =  suggestion(roomCard, weaponCard, suspectCard, p);
		
		if (resultOfSuggestion == -1){
			this._gui.suggestionFail();
			String unused3 = this._reader.next();
			return;
		}
		
		//Player who must prove the suggestion wrong because they are next in order and have one or more matching cards. 
		Player mustProveWrong = this._players.get(resultOfSuggestion);
		
		List<Card> suggestionChoices = this.suggestionResults(roomCard, weaponCard, suspectCard, mustProveWrong);
		
		//Index starts at zero because we are computer scientists
		for (int i = 0; i < suggestionChoices.size(); i++){
			int index = i;
			System.out.println(index + ". " + suggestionChoices.get(i).toString());
		}
		
		int cardIndex = 0;
		while (true){
			this._gui.proveWrongPrompt(mustProveWrong);
			cardIndex = _reader.nextInt();
			try{
				this.cardIndexChecker(cardIndex, suggestionChoices);
				break;
			}
			catch(IllegalInputException e){
				System.out.println(e);
			}
		}
		
		Card chosenCard = suggestionChoices.get(cardIndex);
		
		this._gui.displayCardPrompt(chosenCard.toString(), mustProveWrong);
		String unused4 = this._reader.next();
	}
	
	
	/**
	 * Returns the card specified by the roomTile that is either in the envelope or someone's hand. 
	 * 
	 * @param T 	RoomTile that the suggestion is being made from. No exception thrown becuase
	 * 				RoomTile not input by the player. 
	 * @return		Card for the Room specified by the player.
	 */
	public Card roomChecker(RoomTile T){
		String roomString = T.toString();
		for (Card c: this._deck){
			if (c.getName().equals(roomString) && c.getType().equals("Room")){
				return c;
			}
		}
		for (Card c: this._envelope){
			if (c.getName().equals(roomString)){
				return c;
			}
		}
		return null;
	}
	
	/**
	 * Returns the card specified by the accuser that is either in the envelope or someone's hand. 
	 * In this case, it is a string input because the player does not have to be in a room to accuse. 
	 * 
	 * @param s 	Room input by the player. 
	 * @return		Card for the Room specified by the player.
	 */
	public Card roomCheckerAccuse(String s) throws IllegalInputException{
		for (Card c: this._deck){
			if (c.getName().equals(s) && c.getType().equals("Room")){
				return c;
			}
		}
		for (Card c: this._envelope){
			if (c.getName().equals(s) && c.getType().equals("Room")){
				return c;
			}
		}		
		throw new IllegalInputException("This is not a legal input!");
	}
	
	
	
	/**
	 * Returns the suspect card specified by the player that is either in the envelope or someone's hand. 
	 * 
	 * @param s								String input by the player. 
	 * @return								Card for the suspect specified by the player. 						
	 * @throws IllegalInputException		If the card does not exist or if the player input the wrong type of card. 
	 */
	public Card suspectChecker(String s) throws IllegalInputException{
		for (Card c: this._deck){
			if (c.getName().equals(s) && c.getType().equals("Suspect")){
				return c;
			}
		}
		for (Card c: this._envelope){
			if (c.getName().equals(s) && c.getType().equals("Suspect")){
				return c;
			}
		}		
		throw new IllegalInputException("This is not a legal input!");
	}
	
	/**
	 * Returns the weapon card specified by the player that is either in the envelope or someone's hand. 
	 * 
	 * @param s								String input by the player. 
	 * @return								Card for the suspect specified by the player. 						
	 * @throws IllegalInputException		If the card does not exist or if the player input the wrong type of card. 
	 */
	public Card weaponsChecker(String s) throws IllegalInputException{
		for (Card c: this._deck){
			if (c.getName().equals(s) && c.getType().equals("Weapon")){
				return c;
			}
		}
		for (Card c: this._envelope){
			if (c.getName().equals(s) && c.getType().equals("Weapon")){
				return c;
			}
		}		
		throw new IllegalInputException("This is not a legal input!");
	}
	
	
	
	/**
	 * Mehtod used to check to see if the prove-wrong card index is legal. 
	 * 
	 * @param index		Index of the list chosen
	 * @param list		List holding the cards to be chosen. 
	 * @return			The chosen index if it is legal; 
	 */
	public int cardIndexChecker(int index, List<Card> list) throws IllegalInputException{
		if (index > list.size()-1){
			throw new IllegalInputException("The index is too large!");
		}
		if(index < 0){
			throw new IllegalInputException("Can not have negative index!");
		}
		return index;
	}
	

	
	
	
	/**
	 * Attempts to move the piece in one of 4 cardinal directions or through a
	 * secret passage, if illegal, throws IllegalMoveException
	 * 
	 * @param p
	 *            Piece to be moved.
	 * @param s
	 *            Command to be translated, in string form.
	 * @return True if the move went through a secret passage, otherwise false.
	 * @throws IllegalMoveException
	 *             If the input was incorrect or the move was illegal.
	 *             
	 *  @author David Olsen
	 */
	public boolean move(Piece p, String moveDirection) throws IllegalMoveException {
		if (moveDirection == null) {
			throw new IllegalMoveException("How?!?!?!");
		} else if (this.isInRoom(p) && moveDirection.equals("secret")) {
			RoomTile roomTile = (RoomTile) this._gameBoard[p.getRow()][p.getCol()];
			if (!roomTile.hasSecretPassage()) {
				throw new IllegalMoveException("This room tile does not have a secret passage!");
			}
			p.setRow(roomTile.getSecretPassageRow());
			p.setCol(roomTile.getSecretPassageCol());
			return true;
		} else if (moveDirection.equals("w")) {
			Tile to = this._gameBoard[p.getRow() - 1][p.getCol()];
			Tile from = this._gameBoard[p.getRow()][p.getCol()];
			if (!validMove(to, from)) {
				throw new IllegalMoveException("You ran into a wall!");
			} else if (to.canMoveTo()) {
				p.setRow(p.getRow() - 1);
				to.canMoveSet(false);
				from.canMoveSet(true);
				return false;
			} else {
				throw new IllegalMoveException("Can not move up from this position!");
			}
		} else if (moveDirection.equals("s")) {
			Tile to = this._gameBoard[p.getRow() + 1][p.getCol()];
			Tile from = this._gameBoard[p.getRow()][p.getCol()];
			if (!validMove(to, from)) {
				throw new IllegalMoveException("You ran into a wall!");
			} else if (to.canMoveTo()) {
				p.setRow(p.getRow() + 1);
				to.canMoveSet(false);
				from.canMoveSet(true);
				return false;
			} else {
				throw new IllegalMoveException("Can not move down from this position!");
			}
		} else if (moveDirection.equals("a")) {
			Tile to = this._gameBoard[p.getRow()][p.getCol() - 1];
			Tile from = this._gameBoard[p.getRow()][p.getCol()];
			if (!validMove(to, from)) {
				throw new IllegalMoveException("You ran into a wall!");
			} else if (to.canMoveTo()) {
				p.setCol(p.getCol() - 1);
				to.canMoveSet(false);
				from.canMoveSet(true);
				return false;
			} else {
				throw new IllegalMoveException("Can not move left from this position!");
			}
		} else if (moveDirection.equals("d")) {
			Tile to = this._gameBoard[p.getRow()][p.getCol() + 1];
			Tile from = this._gameBoard[p.getRow()][p.getCol()];
			if (!validMove(to, from)) {
				throw new IllegalMoveException("You ran into a wall!");
			} else if (to.canMoveTo()) {
				p.setCol(p.getCol() + 1);
				to.canMoveSet(false);
				from.canMoveSet(true);
				return false;
			} else {
				throw new IllegalMoveException("Can not move right from this position!");
			}
		} else {
			throw new IllegalMoveException("Not a valid direction to move!");
		}
	}

	/**
	 * Helper method for move - determines if a move is valid by checking to see
	 * if two adjacent tiles are movable
	 * 
	 * 
	 * @param t1
	 *            Tile moving from
	 * @param t2
	 *            Tile going to
	 * @return if the move is valid
	 */
	public boolean validMove(Tile t1, Tile t2) {
		if (t1.isRoomTile() && !t2.isRoomTile() && !t2.isDoorTile()) {
			return false;
		}
		if (t2.isRoomTile() && !t1.isRoomTile() && !t1.isDoorTile()) {
			return false;
		}
		return true;
	}

	/**
	 * Determines if a piece is inside of a room.
	 * 
	 * @param p
	 *            Piece instance being tested.
	 * @return If the piece is currently in a room tile, the row/column
	 *         coordinates correspond with a RoomTile.
	 */
	public boolean isInRoom(Piece p) {
		Tile T = this._gameBoard[p.getRow()][p.getCol()];
		return T.isRoomTile();
	}

	/**
	 * 
	 * Used to build a gameboard with walls, tiles, and roomtiles. At this stage
	 * of development, this is for testing purpouses. There are 4 test rooms,
	 * one in each corner, all with secret passages that point to the opposite
	 * corner. There is a ring of tiles that are impassible, technically count
	 * as walls
	 * 
	 */
	public void buildTestGameBoard() {
		for (int i = 0; i < _gameBoard.length; i++) {
			for (int j = 0; j < _gameBoard[i].length; j++) {
				if (i == 0 || i == _gameBoard.length - 1 || j == 0 || j == _gameBoard[i].length - 1) {
					Tile T = new Tile(false);
					_gameBoard[i][j] = T;
					// Put room tiles in 4 corners
				} else if (i == 1 && j == 1) {
					RoomTile T = new RoomTile("Upper Left Test", true, 8, 8);
					_gameBoard[i][j] = T;
				} else if (i == 1 && j == 8) {
					RoomTile T = new RoomTile("Upper Right Test", true, 8, 1);
					_gameBoard[i][j] = T;
				} else if (i == 8 && j == 1) {
					RoomTile T = new RoomTile("Lower Left Test", true, 1, 8);
					_gameBoard[i][j] = T;
				} else if (i == 8 && j == 8) {
					RoomTile T = new RoomTile("Lower Right Test", true, 1, 1);
					_gameBoard[i][j] = T;
				}
				// Put door tiles in 4 corners clockwise to each room tile
				else if (i == 1 && j == 2) {
					DoorTile T = new DoorTile();
					_gameBoard[i][j] = T;
				} else if (i == 2 && j == 8) {
					DoorTile T = new DoorTile();
					_gameBoard[i][j] = T;
				} else if (i == 8 && j == 7) {
					DoorTile T = new DoorTile();
					_gameBoard[i][j] = T;
				} else if (i == 7 && j == 1) {
					DoorTile T = new DoorTile();
					_gameBoard[i][j] = T;
				} else {
					Tile T = new Tile(true);
					_gameBoard[i][j] = T;
				}
			}
		}
	}


	/**
	 * Used to build the gameboard with the same layout as the GUI. 
	 * 
	 * @author Jackson Bell
	 * @author David Olsen
	 */
	public void buildGameBoard(){
		for (int i = 0; i < _gameBoard.length; i++){
            for (int j= 0; j < _gameBoard[i].length; j++){
            	
            	//rooms
            	if((i > 0 && i <= 4) && (j > 0 && j <= 7)){
            		Tile t = new RoomTile("Study", true, 24, 20);
            		_gameBoard[i][j] = t;
            	}            	
            	else if((i >= 7 && i <= 11) && (j > 0 && j <= 7)){
            		Tile t = new RoomTile("Library");
            		_gameBoard[i][j] = t;
            	}            	
            	else if((i >= 13 && i <= 17) && (j > 0 && j <= 6)){
            		Tile t = new RoomTile("Billiard Room");
            		_gameBoard[i][j] = t;
            	}           	
            	else if((i >= 20 && i <= 24) && (j > 0 && j <= 6)){
            		Tile t = new RoomTile("Conservatory", true, 6, 24);
            		_gameBoard[i][j] = t;
            	}            	
            	else if((i > 0 && i <= 7) && (j >= 10 && j <= 15)){
            		Tile t = new RoomTile("Hall");
            		_gameBoard[i][j] = t;
            	}            	
            	else if((i > 0 && i <= 6) && (j >= 18 && j <= 24)){
            		Tile t = new RoomTile("Lounge", true, 20, 2);
            		_gameBoard[i][j] = t;
            	}            	
            	else if((i >= 10 && i <= 15) && (j  >= 17 && j <= 24)){
            		Tile t = new RoomTile("Dining Room");
            		_gameBoard[i][j] = t;
            	}
            	else if((i >= 10 && i <= 16) && (j  >= 20 && j <= 24)){
            		Tile t = new RoomTile("Dining Room");
            		_gameBoard[i][j] = t;
            	}
            	else if((i >= 19 && i <= 24) && (j >= 19 && j <= 24)){
            		Tile t = new RoomTile("Kitchen", true, 4, 1);
            		_gameBoard[i][j] = t;
            	}            	
            	else if((i >= 18 && i <= 25) && (j >= 9 && j <= 16)){
            		Tile t = new RoomTile("Ballroom");
            		_gameBoard[i][j] = t;
            	}            	
            	else if((i >= 9 && i <= 15) && (j >= 10 && j <= 14)){ //basement
            		Tile t = new Tile(false);
            		_gameBoard[i][j] = t;
            	}
        	
            	
            	           	
            	
            	//invalid borders          		
            	else if((i == 0) && (j >= 0 && j <= 26)){
            		Tile t = new Tile(false);
            		_gameBoard[i][j] = t; //border top
            	}
            	else if((i >= 1 && i <= 25) && (j == 0)){
            		Tile t = new Tile(false);
            		_gameBoard[i][j] = t; //border left
            	}
            	else if((i == 25) && (j >= 0 && j < 26)){
            		Tile t = new Tile(false);
            		_gameBoard[i][j] = t; //border bottom
            	}
            	else if((i >= 1 && i < 26) && (j == 25)){
            		Tile t = new Tile(false);
            		_gameBoard[i][j] = t; //border right
            	}
            	else if (i == 26){
            		Tile t = new Tile(false);
            		_gameBoard[i][j] = t;
            	}
            	
            	
            	//hallway tiles
            	else{
            		Tile t = new Tile(true);
            		_gameBoard[i][j] = t;
            	}
            	
            	
       	
            	}
            }
		//doors
    		Tile ta = new DoorTile();
    		_gameBoard[5][7] = ta; //study door
    		
    		Tile tb = new DoorTile();
    		_gameBoard[9][8] = tb; //library door 1
    		
    		Tile tc = new DoorTile();
    		_gameBoard[12][4] = tc; //library door 2
    		
    		Tile td = new DoorTile();
    		_gameBoard[12][2] = td; //billiard room door 1
    		
    		Tile te = new DoorTile();
    		_gameBoard[16][7] = te; //billiard room door 2
    		
    		Tile tf = new DoorTile();
    		_gameBoard[20][6] = tf; //conservatory door
    		
    		Tile tg = new DoorTile();
    		_gameBoard[5][9] = tg; //hall door 1
    		
    		Tile th = new DoorTile();
    		_gameBoard[8][12] = th; //hall door 2
    		
    		Tile ti = new DoorTile();
    		_gameBoard[8][13] = ti; //hall door 3

    		Tile tj = new DoorTile();
    		_gameBoard[7][18] = tj; //lounge door
    		
    		Tile tk = new DoorTile();
    		_gameBoard[9][18] = tk; //dining room door 1
    		
    		Tile tl = new DoorTile();
    		_gameBoard[13][16] = tl; //dining room door 2
    		
    		Tile tm = new DoorTile();
    		_gameBoard[18][20] = tm; //kitchen door
    		
    		Tile tn = new DoorTile();
    		_gameBoard[17][15] = tn; //ballroom door 1
    		
    		Tile to = new DoorTile();
    		_gameBoard[17][10] = to; //ballroom door 2
    		
    		Tile tp = new DoorTile();
    		_gameBoard[20][17] = tp; //ballroom door 3
    		
    		Tile tq = new DoorTile();
    		_gameBoard[20][8] = tq; //ballroom door 4
		
		
		//new tiles for irregularly shaped rooms
    	Tile t1 = new Tile(true);
    	_gameBoard[7][7] = t1;
    	Tile t2 = new Tile(true);
    	_gameBoard[11][7] = t2;
    	Tile t3 = new Tile(true);
    	_gameBoard[7][7] = t3;
    	Tile t4 = new Tile(true);
    	_gameBoard[16][17] = t4;
    	Tile t5 = new Tile(true);
    	_gameBoard[16][18] = t5;
    	Tile t6 = new Tile(true);
    	_gameBoard[16][19] = t6;
    	Tile t7 = new Tile(true);
    	_gameBoard[16][17] = t7;
    	Tile t8 = new Tile(true);
    	_gameBoard[24][9] = t8;
    	Tile t9 = new Tile(true);
    	_gameBoard[24][10] = t9;
    	Tile t10 = new Tile(true);
    	_gameBoard[25][10] = t10;
    	Tile t11 = new Tile(true);
    	_gameBoard[24][15] = t11;
    	Tile t12 = new Tile(true);
    	_gameBoard[25][15] = t12;
    	Tile t13 = new Tile(true);
    	_gameBoard[24][16] = t13;
		
		//more invalid tiles
		Tile T = new Tile(false);
    	_gameBoard[1][7] = T ;
    	_gameBoard[1][9]= T;
    	_gameBoard[5][1]= T;
    	_gameBoard[7][1]= T;
    	_gameBoard[11][1] = T;
    	_gameBoard[12][1]= T;
    	_gameBoard[1][16]= T;
    	_gameBoard[1][18]= T;
    	_gameBoard[7][24]= T;
    	_gameBoard[9][24]= T;
    	_gameBoard[17][24]= T;
    	_gameBoard[19][24]= T;
    	_gameBoard[25][18]= T;
    	_gameBoard[18][1]= T;
    	_gameBoard[20][1]= T;
    	_gameBoard[24][7]= T;
    	_gameBoard[24][18]= T;
	}
	
	
	
	/**
	 * 
	 *
	 * 
	 * Cycles through each of the players' hands and compares their cards with
	 * the cards in the suggestion. If a player has a match, the index number of
	 * that player is returned. The players' hands are searched in order,
	 * starting with the player after the player making the suggestion. If none
	 * of the other players has a match, then -1 is returned.
	 * 
	 * The suggestion consists of three cards, one each for a room, weapon, and
	 * suspect. These cards are the first three inputs. The last input
	 * identifies which player is making the suggestion.
	 * 
	 * @author Elizabeth Stanley
	 * @author Amy Lipsky
	 * 
	 * @param room
	 *            Room Card
	 * @param weapon
	 *            Weapon Card
	 * @param suspect
	 *            Suspect Card
	 * @param p
	 *            Piece making the suggestion
	 * @return i Index in List of Piece making the suggestion
	 */
	public int suggestion(Card room, Card weapon, Card suspect, Player p) {
		int i = _players.indexOf(p) + 1;
		boolean hasAnswer = false;
		while (i != this._players.indexOf(p) && !hasAnswer) {
			if (i >= _players.size()) {
				i = 0;
			}
			// check if the next player has an answer
			Player q = _players.get(i);
			List<Card> h = q.getHand();
			Card w;
			for (int j = 0; j < h.size(); j++) {
				w = h.get(j);
				if (w.equals(room)) {
					hasAnswer = true;
				}
				if (w.equals(weapon)) {
					hasAnswer = true;
				}
				if (w.equals(suspect)) {
					hasAnswer = true;
				}
			}
			// if they have answer, return _players.get(i)
			if (!hasAnswer) {
				i = i + 1;
			}
		}
		if (!hasAnswer) {
			i = -1;
		}
		return i;
	}
	
	
	/**
	 * 
	 * This method is called to allow the person proving the suggestion wrong to pick a card to show. 
	 * 
	 * @param room		Room card used in the suggestion
	 * @param weapon	Weapon card used in the suggestion
	 * @param suspect	Suspect suggested
	 * @param p			Player who has one or more of the cards,
	 * @return			List of all cards the player has who can prove the suggestion wrong. 
	 * 
	 * @author David Olsen
	 */
	public List<Card> suggestionResults(Card room, Card weapon, Card suspect, Player p){
		
		List<Card> returnList = new ArrayList<Card>();
		
		List<Card> playerHand = p.getHand();
		
		for (Card c: playerHand){
			if (c.equals(room) || c.equals(weapon) || c.equals(suspect)){
				returnList.add(c);
			}
		}
		return returnList;
	}
	
	
	

	/**
	 * @return Tile[][] of tiles corresponding to the gameboard.
	 */
	public Tile[][] getGameBoard() {
		return this._gameBoard;
	}

	/**
	 * @return List<Piece> of every piece that has been added to the game.
	 */
	public List<Piece> getPieces() {
		return this._pieces;
	}
	
	/**
	 * @return List<Player> of every player that has been added to the game.
	 */
	public List<Player> getPlayers(){
		return this._players;
	}
	


	
	/**
	 * @return random int between 1 and 6.
	 */
	public int dieRoll() {
		Random r = new Random();
		int test;
		test=r.nextInt(6)+1;
		return test;
	}

	/**
	 * Generates all of the cards and adds them to the envelope and deck. 
	 * 
	 * @author Elizabeth Stanley
	 */
	public void genDeckEnv() {
		Card card;
		Random rand = new Random();
		int r=0;
		r=rand.nextInt(room.length);
		for (int i=0; i<room.length; i++) {
			card = new Card(room[i],"Room");
			if (i==r) {
				_envelope.add(card);
			} else {
				_deck.add(card);
			}
		}
		r=rand.nextInt(suspect.length);
		for (int i=0; i<suspect.length; i++) {
			card = new Card(suspect[i],"Suspect");
			if (i==r) {
				_envelope.add(card);
			} else {
				_deck.add(card);
			}
		}
		r=rand.nextInt(weapon.length);
		for (int i=0; i<weapon.length; i++) {
			card = new Card(weapon[i],"Weapon");
			if (i==r) {
				_envelope.add(card);
			} else {
				_deck.add(card);
			}
		}
	}
	
	/**
	 *  Method to generate all of the game pieces in order. 
	 *  
	 *  @author Elizabeth Stanley
	 */
	public void genPieces() {
		Piece p;
		for (int i=0; i<suspect.length; i++) {
			p = new Piece(suspectRow[i],suspectCol[i],suspect[i]);
			_pieces.add(p);
		}
	}
	
	/**
	 * Adds piece to the list of pieces.
	 * 
	 * @param p 	piece  to be added to the list of players
	 * @return if the player was added to the list of players
	 */
	public boolean addPiece(Piece p) {
		if (!this._pieces.contains(p)) {
			return this._pieces.add(p);
		}
		return false;
	}
	
	
	/**
	 * Adds Player to the list of Players.
	 * 
	 * @param p		player to be added to the list of players
	 * @return if the player was added to the list of players
	 */
	public boolean addPlayer(Player p) {
		if (!this._players.contains(p)) {
			return this._players.add(p);
		}
		return false;
	}
	
	
	/**
	 * Generates all of the players and adds a hand of cards to that player from the deck.
	 * Also associates the correct piece with the appropriate player.
	 * 
	 * We distribute the cards from a copy of the deck to make sure there is a reference to all cards
	 * that is easily accessable. 
	 * 
	 * @param pieceIndices array of indexes for each of the players.
	 * 
	 *  @author Elizabeth Stanley
	 *  @author Amy Lipski
	 */
	public void genPlayers(int[] pieceIndices) {
		Piece p;
		List<Card> hand;
		Player q;
		
		// We are distriuburing 
		List <Card> copyDeck = new ArrayList<Card>();
		for (Card c: _deck){
			copyDeck.add(c);
		}
		
		int numPlayers = pieceIndices.length;
		int numCardsPerHand = _deck.size()/numPlayers;
		int extraCards = _deck.size()%numPlayers; //deals with remaining cards
		int numCards;
        if (numPlayers>=3 && numPlayers<=6) {
			for (int i=0; i<numPlayers; i++) {
				p = _pieces.get(pieceIndices[i]);
				 hand = new ArrayList<Card>();
				
				// Deal hand:
				//   numCards = numCardsPerHand (but add 1 if i < extraCards)
				//   for each card:
				//     pick random integer (card index) from 0 to _deck.length-1
				//     add that card to hand, and remove from deck
				
				// Might make more sense to generate Deck as local var here
				//   (before this "if" block) instead of having "_deck"
				numCards=numCardsPerHand;
				if (i<extraCards) {
					numCards=numCards+1;
				}
				Collections.shuffle(copyDeck);
				for (int j=0; j< numCardsPerHand; j++) {
					hand.add(copyDeck.remove(0));
				}
				q = new Player(p,hand);
				_players.add(q);
			}
		} else {
			throw new IndexOutOfBoundsException();
		}	
	}
	
	/**
	 * Gives next player in sequence.
	 * 
	 * @param q player input
	 * @return next player in sequence
	 */
	public Player turnSequence(Player q) {
		int index=_players.indexOf(q);
		if (index==_players.size()-1) {
			return _players.get(0);
		} else {
			index=index+1;
			return _players.get(index);
		}
	}
	
	
	/**
	 * @author Elizabeth Stanley
	 * @author Amy Lipsky
	 * 
	 * Compares the three cards in the accusation to the cards in the envelope.
	 * If they all match, the player won the game. Otherwise, they lose and can
	 * no longer make any moves.
	 * 
	 * @param room    The room card in the accusation.
	 * @param weapon  The weapon card in the accusation.
	 * @param suspect The suspect card in the accusation.
	 * @param p       The player making the accusation.
	 */
	public void accusation(Card room, Card weapon, Card suspect, Player p) {
		// compare the three cards to the cards in _envelope
		// if they ALL match, the player wins the game (return true)
		// otherwise the player loses their turn (return false)
		
		//Order for _envelope: Room, Suspect, Weapon
		if ( room.equals(_envelope.get(0)) && suspect.equals(_envelope.get(1)) && weapon.equals(_envelope.get(2)) ) {
			//player p won
			p.winGame();
		} else {
			//player p lost
			p.lostGame();
		}
	}
	
	
	/**
	 * @return envelope for testing purpouses
	 */
	public List<Card> getEnvelope() {
		return this._envelope;
	}
	
	/**
	 * @return deck of cards for testing purpouses
	 */
	public List<Card> getDeck() {
		return this._deck;
	}
	
}
