package code;

/**
 * @author David Olsen
 * 
 * This is the class that defines a door tile. 
 * 
 * 
 *
 */
public class DoorTile extends Tile{

	/**
	 *  Constructor for the door tile, is always true. 
	 */
	public DoorTile() {
		super(true);		
	}
	
	/* (non-Javadoc)
	 * @see code.Tile#isDoorTile()
	 * 
	 * this tile is a door tile
	 * 
	 */
	@Override
	public boolean isDoorTile(){
    	return true;
    }
	
	
}