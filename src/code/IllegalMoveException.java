
package code;

/**
 *
 *Definition for IllegalMoveException Class
 *
 * @author David Olsen
 */
public class IllegalMoveException extends Exception{
	/**
	 * @param s string used to pass custom error message
	 */
	IllegalMoveException(String s){
		super(s);
	}
}
