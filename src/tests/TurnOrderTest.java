package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import code.Card;
import code.GameBoard;
import code.Piece;
import code.Player;

/**
 * Test to ensure proper turn order. 
 * 
 * @author Elizabeth Stanley
 *
 */
public class TurnOrderTest {

	@Test
	public void turnOrderScarletPlumTest() {
		GameBoard g = new GameBoard();
		List<Player> q = new ArrayList<Player>();
		g.genPieces();
		g.genDeckEnv();
		int[] ind = {0,1,2,3,4,5};
		g.genPlayers(ind);
		q=g.getPlayers();
		
		Player current = q.get(0); //Miss Scarlet's turn
		Player next    = g.turnSequence(current); //Should be Professor Plum's turn
		String actual = "Professor Plum";
		assertEquals(actual, next.getPiece().toString()); 
	}
	
	@Test
	public void turnOrderPlumGreenTest() {
		GameBoard g = new GameBoard();
		List<Player> q = new ArrayList<Player>();
		g.genPieces();
		g.genDeckEnv();
		int[] ind = {0,1,2,3,4,5};
		g.genPlayers(ind);
		q=g.getPlayers();
		
		Player current = q.get(1); //Professor Plum's turn
		Player next    = g.turnSequence(current); //Should be Mr. Green's turn
		String actual = "Mr. Green";
		assertEquals(actual, next.getPiece().toString()); 
	}
	
	@Test
	public void turnOrderGreenWhiteTest() {
		GameBoard g = new GameBoard();
		List<Player> q = new ArrayList<Player>();
		g.genPieces();
		g.genDeckEnv();
		int[] ind = {0,1,2,3,4,5};
		g.genPlayers(ind);
		q=g.getPlayers();
		
		Player current = q.get(2); //Mr. Green's turn
		Player next    = g.turnSequence(current); //Should be Mrs. White's turn
		String actual = "Mrs. White";
		assertEquals(actual, next.getPiece().toString()); 
	}
	
	@Test
	public void turnOrderWhitePeacockTest() {
		GameBoard g = new GameBoard();
		List<Player> q = new ArrayList<Player>();
		g.genPieces();
		g.genDeckEnv();
		int[] ind = {0,1,2,3,4,5};
		g.genPlayers(ind);
		q=g.getPlayers();
		
		Player current = q.get(3); //Mrs. White's turn
		Player next    = g.turnSequence(current); //Should be Mrs. Peacock's turn
		String actual = "Mrs. Peacock";
		assertEquals(actual, next.getPiece().toString()); 
	}
	
	@Test
	public void turnOrderPeacockMustardTest() {
		GameBoard g = new GameBoard();
		List<Player> q = new ArrayList<Player>();
		g.genPieces();
		g.genDeckEnv();
		int[] ind = {0,1,2,3,4,5};
		g.genPlayers(ind);
		q=g.getPlayers();
		
		Player current = q.get(4); //Mrs. Peacock's turn
		Player next    = g.turnSequence(current); //Should be Colonel Mustard's turn
		String actual = "Colonel Mustard";
		assertEquals(actual, next.getPiece().toString()); 
	}
	
	@Test
	public void turnOrderMustardScarletTest() {
		GameBoard g = new GameBoard();
		List<Player> q = new ArrayList<Player>();
		g.genPieces();
		g.genDeckEnv();
		int[] ind = {0,1,2,3,4,5};
		g.genPlayers(ind);
		q=g.getPlayers();
		
		Player current = q.get(5); //Colonel Mustard's turn
		Player next    = g.turnSequence(current); //Should be Miss Scarlet's turn
		String actual = "Miss Scarlet";
		assertEquals(actual, next.getPiece().toString()); 	
	}

}
