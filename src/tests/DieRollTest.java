package tests;

import static org.junit.Assert.*;
import java.util.*;

import org.junit.Test;
import code.GameBoard;

/**
 * Simple die roll test. 
 * 
 * 
 * @author Elizabeth Stanley
 *
 */
public class DieRollTest {

	@Test
	public void dieRollTest() {
		GameBoard t = new GameBoard();
		double iter=6*1000.0; //need it to be divisible by 6!
		int test=0;
		int[] count={0,0,0,0,0,0,0};
		double stdev=0.0;
		double mean=0.0;
		
		for (int i=0;i<iter;i++) {
			test=t.dieRoll();
			
			switch (test) {
				case 1:
					count[1]=count[1]+1;
					break;
				case 2:
					count[2]=count[2]+1;
					break;
				case 3:
					count[3]=count[3]+1;
					break;
				case 4:
					count[4]=count[4]+1;
					break;
				case 5:
					count[5]=count[5]+1;
					break;
				case 6:
					count[6]=count[6]+1;
					break;
				default:
					count[0]=count[0]+1;
					break;
			}
		}
		// mean and standard deviation
		//   binomial distribution with n=iter, p=1/6, and q=5/6
		mean=iter*(1.0/6.0);
		stdev=Math.sqrt(iter*(1.0/6.0)*(5.0/6.0));
		
		// Approximating as a Gaussian Distribution
		//   99.9% of the time, the result will be within 3.29 standard deviations
		assertEquals("Number of 1s is outside bounds!",mean,count[1],3.29*stdev);
		assertEquals("Number of 2s is outside bounds!",mean,count[2],3.29*stdev);
		assertEquals("Number of 3s is outside bounds!",mean,count[3],3.29*stdev);
		assertEquals("Number of 4s is outside bounds!",mean,count[4],3.29*stdev);
		assertEquals("Number of 5s is outside bounds!",mean,count[5],3.29*stdev);
		assertEquals("Number of 6s is outside bounds!",mean,count[6],3.29*stdev);
		assertEquals("Error outside of bounds showed up!",0,count[0]);
	}
}
