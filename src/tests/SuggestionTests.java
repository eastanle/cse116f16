package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import code.*;

/**
 * All tests use the same seven cards.
 * Three of the cards the room, weapon, and suspect of the suggestion.
 * The other four cards are "extra". One is "dealt" to each of the players
 * so that they don't have an empty hand.
 * 
 * All tests use the same four players: Miss Scarlet
 *                                      Professor Plum
 *                                      Mrs. Peacock
 *                                      Mr. Green
 * 
 * In each test, the four players are each dealt their "extra" card,
 * and one or two additional cards, depending on what is being tested.
 * 
 * 
 * 
 * @author Elizabeth Stanley
 * 
 *  
 * */

public class SuggestionTests {

    @Test
    public void suggestionResponseTestPlayer() {
    	GameBoard game = new GameBoard();
    	
    	//Suggestion would be answered by the next player because they have the Player card
       	//Suggestion is:
    	Card room=new Card("Kitchen", "Room");
    	Card weapon=new Card("Wrench", "Weapon");
    	Card suspect=new Card("Mr. Green", "Suspect");
    	Card ex1=new Card("Rope", "Weapon");
    	Card ex2=new Card("Study", "Room");
    	Card ex3=new Card("Mrs. White", "Suspect");
    	Card ex4=new Card("Lounge", "Room");
    	
    	//Players
    	//Plum makes suggestion, Peacock should be the one answering
    	Piece p = new Piece(17,  1, "Miss Scarlet");
    	Player p1 = new Player(p);
    	boolean b = p1.addCardToHand(ex1);
        game.addPlayer(p1);
        
        p=new Piece(20,23,"Professor Plum"); //Plum's in the Kitchen
        p1 = new Player(p);
        b = p1.addCardToHand(ex2);
        game.addPlayer(p1);
        
        p=new Piece( 1,19,"Mrs. Peacock");
        p1 = new Player(p);
        b = p1.addCardToHand(suspect); //Peacock has the suspect card
        b = p1.addCardToHand(ex3);
        game.addPlayer(p1);
        
        p=new Piece(10,25,"Mr. Green");
        p1 = new Player(p);
        b = p1.addCardToHand(ex4);
        game.addPlayer(p1);
        
        p1 = game.getPlayers().get(1); //Plum
        int response = game.suggestion(room, weapon, suspect, p1);
        assertEquals(2,response);
    }
    
    @Test
    public void suggestionResponseTestRoom() {
    	
    	GameBoard game = new GameBoard();
    	
    	//Suggestion would be answered by the next player because they have the Room card
    	//Suggestion is:
    	Card room=new Card("Kitchen", "Room");
    	Card weapon=new Card("Wrench", "Weapon");
    	Card suspect=new Card("Mr. Green", "Suspect");
    	Card ex1=new Card("Rope", "Weapon");
    	Card ex2=new Card("Study", "Room");
    	Card ex3=new Card("Mrs. White", "Suspect");
    	Card ex4=new Card("Lounge", "Room");
    	
    	//Players
    	//Plum makes suggestion, Peacock should be the one answering
    	Piece p = new Piece(17,  1, "Miss Scarlet");
    	Player p1 = new Player(p);
    	boolean b = p1.addCardToHand(ex1);
        game.addPlayer(p1);
        
        p=new Piece(20,23,"Professor Plum"); //Plum's in the Kitchen
        p1 = new Player(p);
        b = p1.addCardToHand(ex2);
        game.addPlayer(p1);
        
        p=new Piece( 1,19,"Mrs. Peacock");
        p1 = new Player(p);
        b = p1.addCardToHand(room); //Peacock has the room card
        b = p1.addCardToHand(ex3);
        game.addPlayer(p1);
        
        p=new Piece(10,25,"Mr. Green");
        p1 = new Player(p);
        b = p1.addCardToHand(ex4);
        game.addPlayer(p1);
                
        p1 = game.getPlayers().get(1); //Plum
        
        int response = game.suggestion(room, weapon, suspect, p1);
        assertEquals(2,response);
    }
    @Test
    public void suggestionResponseTestWeapon() {
    	
    	GameBoard game = new GameBoard();
    	
    	//Suggestion would be answered by the next player because they have the Weapon card
    	//Suggestion is:
    	Card room=new Card("Kitchen", "Room");
    	Card weapon=new Card("Wrench", "Weapon");
    	Card suspect=new Card("Mr. Green", "Suspect");
    	Card ex1=new Card("Rope", "Weapon");
    	Card ex2=new Card("Study", "Room");
    	Card ex3=new Card("Mrs. White", "Suspect");
    	Card ex4=new Card("Lounge", "Room");
    	
    	//Players
    	//Plum makes suggestion, Peacock should be the one answering
    	Piece p = new Piece(17,  1, "Miss Scarlet");
    	Player p1 = new Player(p);
    	boolean b = p1.addCardToHand(ex1);
        game.addPlayer(p1);
        
        p=new Piece(20,23,"Professor Plum"); //Plum's in the Kitchen
        p1 = new Player(p);
        b = p1.addCardToHand(ex2);
        game.addPlayer(p1);
        
        p=new Piece( 1,19,"Mrs. Peacock");
        p1 = new Player(p);
        b = p1.addCardToHand(weapon); //Peacock has the weapon card
        b = p1.addCardToHand(ex3);
        game.addPlayer(p1);
        
        p=new Piece(10,25,"Mr. Green");
        p1 = new Player(p);
        b = p1.addCardToHand(ex4);
        game.addPlayer(p1);

        p1 = game.getPlayers().get(1); //Plum
        int response = game.suggestion(room, weapon, suspect, p1);
        assertEquals(2,response);
    }
    @Test
    public void suggestionResponseTestTwo() {
    	
    	GameBoard game = new GameBoard();
    	
    	//Suggestion would be answered by the next player because they have 2 matching cards
    	//Suggestion is:
    	Card room=new Card("Kitchen", "Room");
    	Card weapon=new Card("Wrench", "Weapon");
    	Card suspect=new Card("Mr. Green", "Suspect");
    	Card ex1=new Card("Rope", "Weapon");
    	Card ex2=new Card("Study", "Room");
    	Card ex3=new Card("Mrs. White", "Suspect");
    	Card ex4=new Card("Lounge", "Room");
    	
    	//Players
    	//Plum makes suggestion, Peacock should be the one answering
    	Piece p = new Piece(17,  1, "Miss Scarlet");
    	Player p1 = new Player(p);
    	boolean b = p1.addCardToHand(ex1);
        game.addPlayer(p1);
        
        p=new Piece(20,23,"Professor Plum"); //Plum's in the Kitchen
        p1 = new Player(p);
        b = p1.addCardToHand(ex2);
        game.addPlayer(p1);
        
        p=new Piece( 1,19,"Mrs. Peacock");
        p1 = new Player(p);
        b = p1.addCardToHand(suspect); //Peacock has the suspect card
        b = p1.addCardToHand(room); //Peacock also has the room card
        b = p1.addCardToHand(ex3);
        game.addPlayer(p1);
        
        p=new Piece(10,25,"Mr. Green");
        p1 = new Player(p);
        b = p1.addCardToHand(ex4);
        game.addPlayer(p1);
        
        p1 = game.getPlayers().get(1); //Plum
        int response = game.suggestion(room, weapon, suspect, p1);
        assertEquals(2,response);
    }
    @Test
    public void suggestionResponseTestSecond() {
    	
    	GameBoard game = new GameBoard();
    	
    	//Suggestion would be answered by the player after the next player because they have 1 or more matching cards
    	//Suggestion is:
    	Card room=new Card("Kitchen", "Room");
    	Card weapon=new Card("Wrench", "Weapon");
    	Card suspect=new Card("Mr. Green", "Suspect");
    	Card ex1=new Card("Rope", "Weapon");
    	Card ex2=new Card("Study", "Room");
    	Card ex3=new Card("Mrs. White", "Suspect");
    	Card ex4=new Card("Lounge", "Room");
    	
    	//Players
    	//Plum makes suggestion, Green should be the one answering
    	Piece p = new Piece(17,  1, "Miss Scarlet");
    	Player p1 = new Player(p);
        boolean b = p1.addCardToHand(ex1);
    	game.addPlayer(p1);
    	
        p=new Piece(20,23,"Professor Plum"); //Plum's in the Kitchen
        p1 = new Player(p);
        b = p1.addCardToHand(ex2);
        game.addPlayer(p1);
        
        p=new Piece( 1,19,"Mrs. Peacock");
        p1 = new Player(p);
        b = p1.addCardToHand(ex3);
        game.addPlayer(p1);
        
        p=new Piece(10,25,"Mr. Green");
        p1 = new Player(p);
        b = p1.addCardToHand(suspect); //Green has the suspect card
        b = p1.addCardToHand(ex4);
        game.addPlayer(p1);
       
        p1 = game.getPlayers().get(1); //Plum
        int response = game.suggestion(room, weapon, suspect, p1);
        assertEquals(3,response);
    }
    @Test
    public void suggestionResponseTestLast() {
    	
    	GameBoard game = new GameBoard();
    	
    	//Suggestion would be answered by the player immediately before player making suggestion because they have 1 or more matching cards
    	//Suggestion is:
    	Card room=new Card("Kitchen", "Room");
    	Card weapon=new Card("Wrench", "Weapon");
    	Card suspect=new Card("Mr. Green", "Suspect");
    	Card ex1=new Card("Rope", "Weapon");
    	Card ex2=new Card("Study", "Room");
    	Card ex3=new Card("Mrs. White", "Suspect");
    	Card ex4=new Card("Lounge", "Room");
    	
    	//Players
    	//Plum makes suggestion, Scarlet should be the one answering
    	Piece p = new Piece(17,  1, "Miss Scarlet");
    	Player p1 = new Player(p);
    	boolean b = p1.addCardToHand(suspect); //Scarlet has the suspect card
    	b = p1.addCardToHand(ex1);
    	game.addPlayer(p1);
    	
        p=new Piece(20,23,"Professor Plum"); //Plum's in the Kitchen
        p1 = new Player(p);
        b = p1.addCardToHand(ex2);
        game.addPlayer(p1);
        
        p=new Piece( 1,19,"Mrs. Peacock");
        p1 = new Player(p);
        b = p1.addCardToHand(ex3);
        game.addPlayer(p1);
        
        p=new Piece(10,25,"Mr. Green");
        p1 = new Player(p);
        b = p1.addCardToHand(ex4);
        game.addPlayer(p1);
        
        p1 = game.getPlayers().get(1); //Plum
        int response = game.suggestion(room, weapon, suspect, p1);
        assertEquals(0,response);
    }
    @Test
    public void suggestionResponseTestOwn() {
    	
    	GameBoard game = new GameBoard();
    	
    	//Suggestion cannot be answered by any player but the player making the suggestion has 1 or more matching cards
    	//Suggestion is:
    	Card room=new Card("Kitchen", "Room");
    	Card weapon=new Card("Wrench", "Weapon");
    	Card suspect=new Card("Mr. Green", "Suspect");
    	Card ex1=new Card("Rope", "Weapon");
    	Card ex2=new Card("Study", "Room");
    	Card ex3=new Card("Mrs. White", "Suspect");
    	Card ex4=new Card("Lounge", "Room");
    	
    	//Players
    	//Plum makes suggestion, no one should be answering
    	Piece p = new Piece(17,  1, "Miss Scarlet");
    	Player p1 = new Player(p);
    	boolean b = p1.addCardToHand(ex1);
        game.addPlayer(p1);
        
        p=new Piece(20,23,"Professor Plum");  //Plum's in the Kitchen
        p1 = new Player(p);
        b = p1.addCardToHand(suspect); //Plum has the suspect card
        b = p1.addCardToHand(ex2);
        game.addPlayer(p1);
        
        p=new Piece( 1,19,"Mrs. Peacock");
        p1 = new Player(p);
        b = p1.addCardToHand(ex3);
        game.addPlayer(p1);
        
        p=new Piece(10,25,"Mr. Green");
        p1 = new Player(p);
        b = p1.addCardToHand(ex4);
        game.addPlayer(p1);
        
        p1 = game.getPlayers().get(1); //Plum
        int response = game.suggestion(room, weapon, suspect, p1);
        assertEquals(-1,response);
    }
    @Test
    public void suggestionResponseTestNone() {
    	
    	GameBoard game = new GameBoard();
    	
    	//Suggestion cannot be answered by any player and the player making the suggestion does not have any matching cards
    	//Suggestion is:
    	Card room=new Card("Kitchen", "Room");
    	Card weapon=new Card("Wrench", "Weapon");
    	Card suspect=new Card("Mr. Green", "Suspect");
    	Card ex1=new Card("Rope", "Weapon");
    	Card ex2=new Card("Study", "Room");
    	Card ex3=new Card("Mrs. White", "Suspect");
    	Card ex4=new Card("Lounge", "Room");
    	
    	//Players
    	//Plum makes suggestion, no one should be answering
    	Piece p = new Piece(17,  1, "Miss Scarlet");
    	Player p1 = new Player(p);
    	boolean b = p1.addCardToHand(ex1);
    	game.addPlayer(p1);
    	
        p=new Piece(20,23,"Professor Plum"); //Plum's in the Kitchen
        p1 = new Player(p);
        b = p1.addCardToHand(ex2);
        game.addPlayer(p1);
        
        p=new Piece( 1,19,"Mrs. Peacock");
        p1 = new Player(p);
        b = p1.addCardToHand(ex3);
        game.addPlayer(p1);
        
        p=new Piece(10,25,"Mr. Green");
        p1 = new Player(p);
        b = p1.addCardToHand(ex4);
        game.addPlayer(p1);

        p1 = game.getPlayers().get(1); //Plum
        int response = game.suggestion(room, weapon, suspect, p1);
        assertEquals(-1,response);
    }
    
    
    @Test 
	public void cardTest(){
    	Card c = new Card("Test Name", "Test Type");
    	assertEquals("Test Name", c.getName());
    	assertEquals("Test Type", c.getType());
    	assertEquals(" name: Test Name type: Test Type ", c.toString());
    }
    
    @Test 
    public void pieceTest(){
    	Piece p = new Piece(1,  1, "Miss Scarlet");
    	
    	Player p1 = new Player(p);
    	
    	Card c = new Card("Test Name", "Test Type");
    	
    	p1.addCardToHand(c);
    	//check to see that you can't add a card to a hand twice
    	assertFalse(p1.addCardToHand(c));
    	assertEquals("Miss Scarlet", p.toString());
    }
    
    

}