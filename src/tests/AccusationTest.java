package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import code.Card;
import code.GameBoard;
import code.Piece;
import code.Player;

/**
 * Tests to see if the player making an accusation wins or loses the game.
 *   For a win, getWin() is true and getLost() is false.
 *   For a loss, getWin() is false and getLost() is true.
 * 
 * @author Elizabeth Stanley
 */

public class AccusationTest {
	
	@Test
	public void typeTest() {
		GameBoard g = new GameBoard();
		List<Player> q = new ArrayList<Player>();
		g.genPieces();
		g.genDeckEnv();
		int[] ind = {0,1,2,3,4,5};
		g.genPlayers(ind);
		q=g.getPlayers();
		
		List<Card> env = g.getEnvelope();
		String c1 = env.get(0).getType(); //should be room
		String c2 = env.get(1).getType(); //should be suspect
		String c3 = env.get(2).getType(); //should be weapon
		
		String room = "Room";
		String suspect = "Suspect";
		String weapon = "Weapon";
		
		assertEquals(room, c1);
		assertEquals(suspect, c2);
		assertEquals(weapon, c3);
		
	}
	
	/**
	 * Tests to see that a player wins when their cards in the accusation match the cards in the envelope.
	 */
	@Test
	public void accusationSuccessTest() {
		//for the purpose of this test, set the player's cards equal to the envelope's cards
		GameBoard g = new GameBoard();
		List<Player> q = new ArrayList<Player>();
		g.genPieces();
		g.genDeckEnv();
		int[] ind = {0,1,2,3,4,5};
		g.genPlayers(ind);
		q=g.getPlayers();
		
		Player a = q.get(0);
		List <Card> h = a.getHand();
		Piece p = a.getPiece();
		while (!h.isEmpty()) {
			h.remove(0);
		}
		
		//for the purpose of this test, set the player's cards equal to the envelope's cards
		for (int i=0; i<3; i++) {
			h.add(g.getEnvelope().get(i));
		}
		Player na=new Player (p,h);
		Card room = na.getHand().get(0);
		Card suspect = na.getHand().get(1);
		Card weapon = na.getHand().get(2);
		g.accusation(room, weapon, suspect, na);
		assertTrue(na.getWin());
		assertFalse(na.getLost());
	}
	
	/**
	 * Tests to see that a player loses when their cards in the accusation DON'T match the cards in the envelope.
	 */
	@Test
	public void accusationFailTest() {
		GameBoard g = new GameBoard();
		List<Player> q = new ArrayList<Player>();
		g.genPieces();
		g.genDeckEnv();
		int[] ind = {0,1,2,3,4,5};
		g.genPlayers(ind);
		q=g.getPlayers();
		
		Player a = q.get(0);
		List <Card> h = a.getHand();
		Piece p = a.getPiece();
		while (!h.isEmpty()) {
			h.remove(0);
		}
		
		//add at least one card definitely NOT in the envelope
		Card c0 = g.getEnvelope().get(0);
		Card c1 = g.getEnvelope().get(1);
		Card c2 = g.getEnvelope().get(2);

		//Ensures that none of the cards match
		for (int i=0; i < g.getDeck().size(); i++) {
			Card temp = g.getDeck().get(i);
			if (h.size() < 3 && !temp.equals(c0) && !temp.equals(c1) && !temp.equals(c2)){
				h.add(temp);
			}
		}
		
		Player na=new Player (p,h);
		Card room = na.getHand().get(0);
		Card suspect = na.getHand().get(1);
		Card weapon = na.getHand().get(2);
		g.accusation(room, weapon, suspect, na);
		assertFalse(na.getWin());
		assertTrue(na.getLost());
	}
}
