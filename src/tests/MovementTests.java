package tests;

import code.*;
import java.util.*;
import org.junit.*;
import static org.junit.Assert.*;



/**
*
* 
* All tests use a GameBoard of size 10 by 10 Tiles, with a ring of "wall" tiles 
* around the edges. 
* There are 8 by 8 tiles that allows legal movement. 
* There are 4 Test Rooms at all four corners of the legal board
* 
* 
* Only the first two tests are commented, because every other test is based off 
* the same format. Only when new things are added are they commented. 
* 
* Allowed inputs are "w" for upward movement, "s" for downward movement, 
* "a" for leftward movement, "d" for rightward movement, and "secret" to move through a 
* secret passage
* 
* 
* 
* Below is a diagram that illustrates the test game board:
* 
* O - tile that is switched 'off'
* T - tile that is switched 'on'
* R - room tile
* D - door tile
* 
* 
*O  O  O  O  O  O  O  O  O  O 
 O  R  D  T  T  T  T  T  R  O 
 O  T  T  T  T  T  T  T  D  O 
 O  T  T  T  T  T  T  T  T  O 
 O  T  T  T  T  T  T  T  T  O 
 O  T  T  T  T  T  T  T  T  O 
 O  T  T  T  T  T  T  T  T  O 
 O  D  T  T  T  T  T  T  T  O 
 O  R  T  T  T  T  T  D  R  O 
 O  O  O  O  O  O  O  O  O  O 
* 
* 
* 
* 
* 
* 
* @author David Olsen
* @author Jackson Bell
* 
*/
public class MovementTests {
	
    
    
    @Test
    public void horizontalTestRight(){
    	
        // Gameboard generates a 2d matrix of tiles size 10 by 10
        GameBoard game = new GameBoard();
        
        // Generate test game board
        game.buildTestGameBoard();
        
        // simulate random die roll and input of horizontal movement
        Random random = new Random();
        
        //Random integer between 1 and 6
        int movements = random.nextInt(6) + 1;
        
        //initialize list of input movements
        List<String> inputs = new ArrayList<String>();
        
        //generate inputs ("d") for rightward movement
        for (int i=0; i<movements; i++){
            inputs.add("d");
        }
        
        // Place a piece at row 4, column 2
        Piece p = new Piece(4, 2, "Test Player");
        
        // add piece to gameboard
        game.addPiece(p);
        
        // Test should always pass no matter what the die rolls, as there are more than
        // 6 horizontal tiles from this starting position        
		boolean tookSecretPassage = false;
		for (String s : inputs) {
			try {
				tookSecretPassage = game.move(p, s);
			} 
			catch (IllegalMoveException e) {
				fail();
			}
		}
                
        // Piece column coordinates should now be equal to 2 + random
        assertEquals(2 + movements, p.getCol());
        
        // Piece row coordinates should be unchanged
        assertEquals(4, p.getRow());
        
        //Check to confirm that the piece is not in a room
        assertFalse(game.isInRoom(p));
        //Check to confirm the player did not take the secret passage
        assertFalse(tookSecretPassage);
    }
    
    @Test
    public void horizontalTestLeft() {
    	
        // Gameboard generates a 2d matrix of tiles size 10 by 10
        GameBoard game = new GameBoard();
        
        // Generate test game board
        game.buildTestGameBoard();

        // simulate random die roll and input of horizontal movement
        Random random = new Random();
        
        //Random integer between 1 and 6
        int movements = random.nextInt(6) + 1;
        
        //initialize list of input movements
        List<String> inputs = new ArrayList<String>();
        
        //generate inputs ("a") for leftward movement
        for (int i=0; i<movements; i++){
            inputs.add("a");
        }
        
        // Place a piece at row 4, column 8
        Piece p = new Piece(4, 8, "Test Player");
        
        // add piece to gameboard
        game.addPiece(p);
        
        // Test should always pass no matter what the die rolls, as there are more than
        // 6 horizontal tiles from this starting position    
		boolean tookSecretPassage = false;
		for (String s : inputs) {
			try {
				tookSecretPassage = game.move(p, s);
			} 
			catch (IllegalMoveException e) {
				fail();
			}
		}        
        // Piece column coordinates should now be equal to 2 + random
        assertEquals(8 - movements, p.getCol());
        
        // Piece row coordinates should be unchanged
        assertEquals(4, p.getRow());
        
        //Check to confirm that the piece is not in a room
        assertFalse(game.isInRoom(p));
        //Check to confirm the player did not take the secret passage
        assertFalse(tookSecretPassage);
    }
    
    @Test
    public void verticalTestUp() {
        GameBoard game = new GameBoard();
        game.buildTestGameBoard();


        Random random = new Random();

        int movements = random.nextInt(6) + 1;

        List<String> inputs = new ArrayList<String>();
        //generate inputs ("w") for upward movement
        for (int i = 0; i < movements; i++) {
            inputs.add("w");
        }
        Piece p = new Piece(8,2,"Test Player");
        game.addPiece(p);
        
        boolean tookSecretPassage = false;
		for (String s : inputs) {
			try {
				tookSecretPassage = game.move(p, s);
			} 
			catch (IllegalMoveException e) {
				fail();
			}
		}     
        assertEquals(2, p.getCol());
        assertEquals(8 - movements, p.getRow());
        assertFalse(game.isInRoom(p));
        assertFalse(tookSecretPassage);
    }

    @Test
    public void verticalTestDown() {
        GameBoard game = new GameBoard();
        game.buildTestGameBoard();

        Random random = new Random();

        int movements = random.nextInt(6) + 1;

        List<String> inputs = new ArrayList<String>();
        //generate inputs ("s") for downward movement
        for (int i = 0; i < movements; i++) {
            inputs.add("s");
        }
        Piece p = new Piece(2,2,"Test Player");
        game.addPiece(p);

        boolean tookSecretPassage = false;
		for (String s : inputs) {
			try {
				tookSecretPassage = game.move(p, s);
			} 
			catch (IllegalMoveException e) {
				fail();
			}
		}   
        assertEquals(2, p.getCol());
        assertEquals(2 + movements, p.getRow());
        assertFalse(game.isInRoom(p));
        assertFalse(tookSecretPassage);
    }

    @Test
    public void vertAndHorzTest() {
        GameBoard game = new GameBoard();
        game.buildTestGameBoard();
        
        //for this test case, hard-code movements to maximum amount allowed
        int movements = 6;

        List<String> inputs = new ArrayList<String>();

        for (int i = 0; i < movements; i++) {
            //Alternate between "s" and "d" testing for vert+horiz movement
            if (i%2 == 0){
                inputs.add("s");
            }
            else{
                inputs.add("d");
            }
        }
        Piece p = new Piece(1,2,"Test Player");
        game.addPiece(p);

        boolean tookSecretPassage = false;
		for (String s : inputs) {
			try {
				tookSecretPassage = game.move(p, s);
			} 
			catch (IllegalMoveException e) {
				fail();
			}
		}   
        assertEquals(5, p.getCol());
        assertEquals(4, p.getRow());
        assertFalse(game.isInRoom(p));
        assertFalse(tookSecretPassage);
    }
    
    
    @Test
    public void doorTest() {
        GameBoard game = new GameBoard();
        game.buildTestGameBoard();
        
        //for this test case, hard-code movements to enter door, Door will be one position left of the piece("a")
        List<String> inputs = new ArrayList<String>();
        inputs.add("a");
        inputs.add("a");
        
        Piece p = new Piece(1,3,"Test Player");
        game.addPiece(p);

        boolean tookSecretPassage = false;
		for (String s : inputs) {
			try {
				tookSecretPassage = game.move(p, s);
			} 
			catch (IllegalMoveException e) {
				fail();
			}
		}  
        assertEquals(1, p.getCol());
        assertEquals(1, p.getRow());
        //Check to confirm that the piece is in a room
        assertTrue(game.isInRoom(p));
        assertFalse(tookSecretPassage);
    }
    
    
    @Test
    public void passageWayTest() {
        GameBoard game = new GameBoard();
        game.buildTestGameBoard();
        
        List<String> inputs = new ArrayList<String>();
        // hardcode word to pass if in door
        inputs.add("secret");
        
        //place player in room to begin
        Piece p = new Piece(1,1,"Test Player");
        game.addPiece(p);
        
        boolean tookSecretPassage = false;
		for (String s : inputs) {
			try {
				tookSecretPassage = game.move(p, s);
			} 
			catch (IllegalMoveException e) {
				fail();
			}
		}  
        // Tile where player took secret passage from - safe to do this because this tile was predefined as a RoomTile
        RoomTile roomTile = (RoomTile)game.getGameBoard()[1][1];
        
        // each RoomTile with a secret passage contains the coordinates for that secret
        // passage in it's field  data
        assertEquals(roomTile.getSecretPassageCol(), p.getCol());
        assertEquals(roomTile.getSecretPassageRow(), p.getRow());
        //Check to confirm that the piece is in a room
        assertTrue(game.isInRoom(p));
        //Check to confirm player took secret passage
        assertTrue(tookSecretPassage);
    }
    
    
    @Test
    public void moreThanRollTest() {
        // Code only executes for how many moves are input into the method. 
        // Move method only moves pieces to adjacent squares, or through secret passages, otherwise IllegalMoveException thrown.
        // Impossible to move for more squares than the input, so no feasible test. 
    }
    
    
    @Test
    public void diagonalTest() {
        // Move method only moves pieces to adjacent squares, or through secret passages, otherwise IllegalMoveException thrown.
        // Any input not w/s/a/d or secret will throw this exception, so no feasible test. 
    }
    
    
    @Test
    public void contiguousTest() {
        // Move method only moves pieces to adjacent squares, or through secret passages, otherwise IllegalMoveException thrown.
        // Impossible to move non-contiguously because move method only moves contiguously.
        // Any input not w/s/a/d or secret will throw this exception, so no feasible test. 
    }
    
    
    @Test
    public void wallTest() {
        GameBoard game = new GameBoard();
        game.buildTestGameBoard();

        List<String> inputs = new ArrayList<String>();
        // piece is instructed to move up one row, into wall, throwing exception
        inputs.add("w");
        
        Piece p = new Piece(1,5,"Test Player");
        game.addPiece(p);
        
        // If exception is not thrown, fail, otherwise pass!
        for (String s : inputs) {
			try {
				game.move(p, s);
				fail();
			} 
			catch (IllegalMoveException e) {
				assertEquals("Can not move up from this position!", e.getMessage());
				// make sure the piece did not move
				assertEquals(5, p.getCol());
		        assertEquals(1, p.getRow());
			}
		}  
    }
    
    // Tests for full coverage below this point - designed to test every single possible error output
    @Test
    public void moveNullTest(){
    	GameBoard game = new GameBoard();
        game.buildTestGameBoard();

        List<String> inputs = new ArrayList<String>();
        // piece is instructed to move null, throwing exception
        inputs.add(null);
        
        Piece p = new Piece(1,1,"Test Player");
        game.addPiece(p);
        
        // If exception is not thrown, fail, otherwise pass!
        for (String s : inputs) {
			try {
				game.move(p, s);
				fail();
			} 
			catch (IllegalMoveException e) {
				assertEquals("How?!?!?!", e.getMessage());
				// make sure the piece did not move
				assertEquals(1, p.getCol());
		        assertEquals(1, p.getRow());
			}
		}  
    }
    
    

    
    @Test
    public void wallTest3() {
        GameBoard game = new GameBoard();
        game.buildTestGameBoard();

        List<String> inputs = new ArrayList<String>();
        // piece is instructed to move left one row, into wall, throwing exception
        inputs.add("a");
        
        Piece p = new Piece(2,1,"Test Player");
        game.addPiece(p);
        
        // If exception is not thrown, fail, otherwise pass!
        for (String s : inputs) {
			try {
				game.move(p, s);
				fail();
			} 
			catch (IllegalMoveException e) {
				assertEquals("Can not move left from this position!", e.getMessage());
				// make sure the piece did not move
				assertEquals(1, p.getCol());
		        assertEquals(2, p.getRow());
			}
		}  
    }
    
 
    
    
    @Test
    public void wallTest6() {
        GameBoard game = new GameBoard();
        game.buildTestGameBoard();

        List<String> inputs = new ArrayList<String>();
        // piece is given illegal instruction, throwing exception because trying to move from hallway into room without going through door. 
        inputs.add("w");
        
        Piece p = new Piece(2,1,"Test Player");
        game.addPiece(p);
        
        // If exception is not thrown, fail, otherwise pass!
        for (String s : inputs) {
			try {
				game.move(p, s);
				fail();
			} 
			catch (IllegalMoveException e) {
				assertEquals("You ran into a wall!", e.getMessage());
				// make sure the piece did not move
				assertEquals(1, p.getCol());
		        assertEquals(2, p.getRow());
			}
		}  
    }
    
    
    @Test
    public void incorrectInputTest() {
        GameBoard game = new GameBoard();
        game.buildTestGameBoard();

        List<String> inputs = new ArrayList<String>();
        // piece is given illegal instruction, throwing exception
        inputs.add("asdf");
        
        Piece p = new Piece(5,1,"Test Player");
        game.addPiece(p);
        
        // If exception is not thrown, fail, otherwise pass!
        for (String s : inputs) {
			try {
				game.move(p, s);
				fail();
			} 
			catch (IllegalMoveException e) {
				assertEquals("Not a valid direction to move!", e.getMessage());
				// make sure the piece did not move
				assertEquals(1, p.getCol());
		        assertEquals(5, p.getRow());
			}
		}  
    }
    
    

    
    
    
    @Test
    public void addPlayerTwice() {
        GameBoard game = new GameBoard();
        game.buildTestGameBoard();

        List<String> inputs = new ArrayList<String>();
        // piece is given illegal instruction, throwing exception
        inputs.add("asdf");
        
        Piece p = new Piece(8,1,"Test Player");
        game.addPiece(p);
        assertFalse(game.addPiece(p));
    }
    
    @Test
    public void tileTests(){
    	RoomTile roomtile = new RoomTile("RoomTest");
    	RoomTile roomtile2 = new RoomTile("RoomTest2");
    	Tile T = new Tile(true);
    	
    	assertEquals("RoomTest", roomtile.toString());
    	
    	assertFalse(roomtile.hasSecretPassage());
    	assertFalse(roomtile2.hasSecretPassage());
    	assertFalse(T.hasSecretPassage());
    }
    
    
    
}
